// Fill out your copyright notice in the Description page of Project Settings.


#include "DataExporter.h"

#include "Tests/AutomationTestSettings.h"


FIntPoint UDataExporter::GetIndex(FVector2D position)
{
	//FVector2D Resolutui
	FVector2D distance = position - BottomLeftPosition;
	int x = FMath::RoundToInt((distance.X / Width) * ImageWidth);
	int y = FMath::RoundToInt((distance.Y / Height) * ImageHeight); 
	return FIntPoint(x, y);
}

UDataExporter::Pixel UDataExporter::GetPixel(FVector2D Vector, float Value)
{
	Pixel pixel = Pixel();

	pixel.Value = Value;
	pixel.X = Vector.X;
	pixel.Y = Vector.Y;

	return pixel;
}

int UDataExporter::GetWindow(float Radius)
{
	return FMath::CeilToInt((Radius / Width) * ImageWidth);
}

float UDataExporter::GetValue(float Distance, FVector2D Radius)
{
	int Flag1 = (Distance < Radius.X);
	int Flag2 = (Distance < Radius.X);

	if ((Distance > Radius.Y))
		return 0.0;

	if ((Distance <= Radius.X))
		return 1.0;

	const float LocalDistance = Distance - Radius.X;
	return 1.0-(LocalDistance / (Radius.Y - Radius.X));
}

void UDataExporter::PaintPixel(uint8* Pixels, TArray<FVector2D> Distances, int Diameter, FVector2D Radius, int X, int Y)
{
	// calculate the window considered
	// it's two times the radius
	int HalfValue = Diameter / 2;
	int XPoint = 0, YPoint = 0;
	
	float Distance = 0;
	float Value = 0.0;

	FVector2D WorldPoint = BottomLeftPosition +
		FVector2D((X / (ImageWidth + .0)) * Width,
			(Y / (ImageHeight + .0)) * Height);

	// printf("HELP ME : %d", Distances.Num());
	
	
	for (int i = -HalfValue; i < HalfValue; i++)
	{
		for (int j = -HalfValue; j < HalfValue; j++)
		{
			XPoint = X + j;
			YPoint = Y + i;

			// TODO: Find a way to remove this ifs
			if (XPoint < 0 || YPoint < 0 ||
				XPoint >= ImageWidth || YPoint >= ImageHeight)
				continue;

			Distance = FVector2D::Distance(Distances[YPoint * ImageWidth + XPoint], WorldPoint);
			// Distances[YPoint * ImageWidth + XPoint], Distances[Y * ImageWidth + X]
			
			Value += GetValue(Distance, Radius);
			Value = Value > 1. ? 1. : Value;
		}
	}

	// if (ProcessFlag > 1.0)
	// 	UE_LOG(LogTemp, Log, TEXT( "Processed flag: %f" ), ProcessFlag);	
	uint8 Color = FMath::Floor(Value * 255.0);
	Pixels[Y * 4 * ImageWidth + X * 4 + 0] = Color;
	
	// Pixels[Y * 4 * ImageWidth + X * 4 + 0] = FMath::Clamp<uint8>(Pixels[Y * 4 * ImageWidth + X * 4 + 0], 0, 255);
	Pixels[Y * 4 * ImageWidth + X * 4 + 1] = 0;
	Pixels[Y * 4 * ImageWidth + X * 4 + 2] = 0;
	Pixels[Y * 4 * ImageWidth + X * 4 + 3] = 255;
}

TArray<UDataExporter::Pixel> UDataExporter::GetPoint(FVector2D Position, FVector2D Radius)
{
	TArray<Pixel> PixelArray = TArray<Pixel>();
	
	// static const double PI = 3.1415926535;
	float i, j, angle, x1, y1;
	float r = Radius.X;
	float x = Position.X, y = Position.Y;

	for (j = 0; j < r; j += 0.5)
	{
		for(i = 0; i < 360; i += 0.5f)
		{
			angle = i;
			x1 = j * FMath::Cos(angle * PI / 180);
			y1 = j * FMath::Sin(angle * PI / 180);
			PixelArray.Add(GetPixel(FVector2D(x + x1, y + y1), 1));
		}
	}
	
	return PixelArray;
}


TArray<UDataExporter::Pixel> UDataExporter::GetAllPoints(TArray<FVector2D> Positions, TArray<FVector2D> Radius)
{
	TArray<Pixel> PixelArray = TArray<Pixel>();

	for(int i = 0; i < Positions.Num(); i++)
	{
		TArray<Pixel> TempArray = GetPoint(Positions[i], Radius[i]);
		for (auto pixel : TempArray)
		{
			PixelArray.Add(pixel);
		}
	}
	
	return PixelArray;
}

void UDataExporter::CreateTexture(int width, int height, uint8* Pixels)
{
	// Texture Information
	FString FileName = FString("MyTexture");

	// Create Package
	FString pathPackage = FString("/Game/MyTextures/");
	FString absolutePathPackage = "/MyTextures/";

	FPackageName::RegisterMountPoint(*pathPackage, *absolutePathPackage);
	UPackage * Package = CreatePackage(nullptr, *pathPackage);

	// Create the Texture
	FName TextureName = MakeUniqueObjectName(Package, UTexture2D::StaticClass(), FName(*FileName));
	UTexture2D* Texture = NewObject<UTexture2D>(Package, TextureName, RF_Public | RF_Standalone);

	// Texture Settings
	Texture->PlatformData = new FTexturePlatformData();
	Texture->PlatformData->SizeX = width;
	Texture->PlatformData->SizeY = height;
	Texture->PlatformData->PixelFormat = PF_R8G8B8A8;

	// Passing the pixels information to the texture
	FTexture2DMipMap* Mip = new(Texture->PlatformData->Mips) FTexture2DMipMap();
	Mip->SizeX = width;
	Mip->SizeY = height;
	Mip->BulkData.Lock(LOCK_READ_WRITE);
	uint8* TextureData = (uint8 *) Mip->BulkData.Realloc(height * width * sizeof(uint8)*4);
	FMemory::Memcpy(TextureData, Pixels, sizeof(uint8) * height * width * 4);
	
	Mip->BulkData.Unlock();

	// Updating Texture & mark it as unsaved
	Texture->AddToRoot();
	Texture->UpdateResource();
	// Package->MarkPackageDirty();
	UE_LOG(LogTemp, Log, TEXT( "Texture created: %s" ), &FileName);

	// TODO : Maybe remove this
	free(Pixels);
	Pixels = nullptr;
}

uint8* UDataExporter::CreateDensityArrayOld(TArray<FVector2D> Positions, TArray<FVector2D> Radius) 
{
	uint8 * Pixels = (uint8 *)malloc(Height * Width * 4); 

	// filling the pixels with dummy data (4 boxes: red, green, blue and white)
	for (int y = 0; y < ImageHeight; y++)
	{
		for (int x = 0; x < ImageWidth; x++)
		{
			Pixels[y * 4 * ImageWidth + x * 4 + 0] = 0; // R
			Pixels[y * 4 * ImageWidth + x * 4 + 1] = 0; // G
			Pixels[y * 4 * ImageWidth + x * 4 + 2] = 0; // B
			Pixels[y * 4 * ImageWidth + x * 4 + 3] = 255; // A
		}
	}

	TArray<Pixel> RadiusPoints = GetAllPoints(Positions, Radius);

	for (int i = 0; i < RadiusPoints.Num(); i++)
	{
		Pixel TempPixel = RadiusPoints[i];
		int X = TempPixel.X, Y = TempPixel.Y;

		if (X > ImageWidth || X < 0)
			continue;

		if (Y > ImageHeight || Y < 0)
			continue;
		
		int Value = FMath::RoundToInt(255 * TempPixel.Value);
		Pixels[Y * 4 * ImageWidth + X * 4 + 0] = Value; // R
		Pixels[Y * 4 * ImageWidth + X * 4 + 1] = Value; // G
		Pixels[Y * 4 * ImageWidth + X * 4 + 2] = Value; // B
		Pixels[Y * 4 * ImageWidth + X * 4 + 3] = 255; // A
	}

	return Pixels;
}

uint8* UDataExporter::CreateDensityArray(TArray<FVector2D> Positions, FVector2D Radius)
{
	uint8 * Pixels = (uint8 *)malloc(ImageHeight * ImageWidth * 4);
	TArray<FVector2D> Distances = TArray<FVector2D>(); 
	Distances.Init(FVector2D(10000000, 10000000), ImageHeight * ImageWidth);
	
	
	// for (int y = 0; y < ImageHeight; y++)
	// {
	// 	for (int x = 0; x < ImageWidth; x++)
	// 	{
	// 		Distances[y * ImageWidth + x] = FVector2D(10000000000, 10000000000);
	// 	}
	// }

	TArray<FIntPoint> Points = TArray<FIntPoint>(); 
	
	for (auto Position : Positions)
	{
		 FIntPoint Point = GetIndex(Position);
		
		if (Point.X < 0 || Point.X >= ImageWidth ||
			Point.Y < 0 || Point.Y >= ImageHeight)
				continue;
		
		Points.Add(Point);
		Distances[Point.Y * ImageWidth + Point.X] = Position;
	}

	const int Diameter = GetWindow(Radius.Y) * 2;
	
	// filling the pixels with dummy data (4 boxes: red, green, blue and white)
	for (int y = 0; y < ImageHeight; y++)
	{
		for (int x = 0; x < ImageWidth; x++)
		{
			
			// Pixels[y * 4 * ImageWidth + x * 4 + 0] = 0; // R
			// Pixels[y * 4 * ImageWidth + x * 4 + 1] = 0; // G
			// Pixels[y * 4 * ImageWidth + x * 4 + 2] = 0; // B
			// Pixels[y * 4 * ImageWidth + x * 4 + 3] = 255; // A

			PaintPixel(Pixels, Distances, Diameter, Radius, x, y);
		}
	}

	// for (auto point : Points)
	// {
	// 	FVector2D pos = Distances[point.Y * ImageWidth + point.X];
	// 	FVector2D WorldPoint = BottomLeftPosition +
	// 		FVector2D(((point.X + .0) / (ImageWidth + .0)) * Width,
	// 		((point.Y + .0) / (ImageHeight + .0)) * Height);

	// 	float dist = FVector2D::Distance(WorldPoint, pos);
	// 	UE_LOG(LogTemp, Log, TEXT( "Distance : %f" ), dist + 0.0);
	// 	UE_LOG(LogTemp, Log, TEXT( "Point : %f , %f" ), point.X + 0.0, point.Y + 0.0);
	// 	UE_LOG(LogTemp, Log, TEXT( "position : %f , %f ; %f , %f" ),
	// 		pos.X + 0.0, pos.Y + 0.0, WorldPoint.X + 0.0, WorldPoint.Y + 0.0);

	// 	uint8 color = Pixels[point.Y * 4 * ImageWidth + point.X * 4 + 0];
	// 	Pixels[point.Y * 4 * ImageWidth + point.X * 4 + 0] = 255;
	// 	UE_LOG(LogTemp, Log, TEXT( "Color : %f" ), color + 0.0);
	// }

	// FVector2D WorldPoint = BottomLeftPosition +
	// 		FVector2D((12 / (ImageWidth + .0)) * Width,
	// 		(12 / (ImageHeight + .0)) * Height);

	// FVector2D WorldPoint1 = BottomLeftPosition +
	// 		FVector2D((13 / (ImageWidth + .0)) * Width,
	// 		(12 / (ImageHeight + .0)) * Height);

	// WorldPoint = WorldPoint1 - WorldPoint;

	// UE_LOG(LogTemp, Log, TEXT( "AAAAAAAAAAAAAAAAH : %f , %f" ), WorldPoint.X, WorldPoint.Y);

	return Pixels;
}

UDataExporter::UDataExporter()
{
}

void UDataExporter::CreateTexture(int width, int height)
{
	// Texture Information
	FString FileName = FString("MyTexture");
	
	// x4 because it's RGBA. 4 integers, one for Red, one for Green, one for Blue, one for Alpha
	uint8 * pixels = (uint8 *)malloc(Height * Width * 4); 

	// filling the pixels with dummy data (4 boxes: red, green, blue and white)
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			pixels[y * 4 * width + x * 4 + 0] = 0; // R
			pixels[y * 4 * width + x * 4 + 1] = 0; // G
			pixels[y * 4 * width + x * 4 + 2] = 0; // B
			pixels[y * 4 * width + x * 4 + 3] = 255; // A
		}
	}

	// Create Package
	FString pathPackage = FString("/Game/MyTextures/");
	FString absolutePathPackage = "/MyTextures/";

	FPackageName::RegisterMountPoint(*pathPackage, *absolutePathPackage);

	UPackage * Package = CreatePackage(nullptr, *pathPackage);

	// Create the Texture
	FName TextureName = MakeUniqueObjectName(Package, UTexture2D::StaticClass(), FName(*FileName));
	UTexture2D* Texture = NewObject<UTexture2D>(Package, TextureName, RF_Public | RF_Standalone);

	// Texture Settings
	Texture->PlatformData = new FTexturePlatformData();
	Texture->PlatformData->SizeX = width;
	Texture->PlatformData->SizeY = height;
	Texture->PlatformData->PixelFormat = PF_R8G8B8A8;

	// Passing the pixels information to the texture
	FTexture2DMipMap* Mip = new(Texture->PlatformData->Mips) FTexture2DMipMap();
	Mip->SizeX = width;
	Mip->SizeY = height;
	Mip->BulkData.Lock(LOCK_READ_WRITE);
	uint8* TextureData = (uint8 *) Mip->BulkData.Realloc(height * width * sizeof(uint8)*4);
	FMemory::Memcpy(TextureData, pixels, sizeof(uint8) * height * width * 4);
	
	Mip->BulkData.Unlock();

	// Updating Texture & mark it as unsaved
	Texture->AddToRoot();
	Texture->UpdateResource();
	Package->MarkPackageDirty();

	UE_LOG(LogTemp, Log, TEXT( "Texture created: %s" ), &FileName);

	free(pixels);
	pixels = nullptr;
}

// Function for creating a new texture
void UDataExporter::CreateTexture()
{
	ImageWidth = 1024;
	ImageHeight = 1024;
	CreateTexture(1024, 1024);
}

void UDataExporter::CreateDensityMapOld(TArray<FVector2D> Positions, TArray<FVector2D> Radius, int width, int height,
	FVector2D bottomLeftPosition, FVector2D topRightPosition)
{
	ImageWidth = width;
	ImageHeight = height;

	BottomLeftPosition = bottomLeftPosition;
	TopRightPosition = topRightPosition;

	Width = TopRightPosition.X - BottomLeftPosition.X;
	Height = TopRightPosition.Y - BottomLeftPosition.Y;

	// CreateTexture(width, height, CreateDensityArray(Positions, Radius));
}

void UDataExporter::CreateDensityMap(TArray<FVector2D> Positions, FVector2D Radius, int width, int height,
	FVector2D bottomLeftPosition, FVector2D topRightPosition)
{
	ImageWidth = width;
	ImageHeight = height;

	BottomLeftPosition = bottomLeftPosition;
	TopRightPosition = topRightPosition;

	Width = TopRightPosition.X - BottomLeftPosition.X;
	Height = TopRightPosition.Y - BottomLeftPosition.Y;

	CreateTexture(width, height,CreateDensityArray(Positions, Radius));
}
