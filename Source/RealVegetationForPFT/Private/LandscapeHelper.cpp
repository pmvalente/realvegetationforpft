// Fill out your copyright notice in the Description page of Project Settings.


#include "LandscapeHelper.h"

#include <string>
// #include <UIAutomationCore.h>

#include "Engine.h"
#include "FileHelpers.h"
#include "FoliageEditModule.h"
#include "Editor/FoliageEdit/Private/FoliageEdMode.h"
#include "FoliageEdit/Public/FoliageEditUtility.h"

#include "WorldBrowserModule.h"
#include "Engine/WorldComposition.h"
#include "WorldBrowser/Private/LevelCollectionModel.h"

#include "LandscapeInfo.h"
#include "LandscapeLayerInfoObject.h"
#include "EditorLevelLibrary.h"
#include "LandscapeStreamingProxy.h"
#include "ProceduralFoliageInstance.h"
#include "ProceduralFoliageSpawner.h"
#include "Commandlets/GatherTextCommandlet.h"


ALandscape* ULandscapeHelper::GetLandscape() const
{
	// Get all landscape actors from the scene
	TArray<AActor*> Lands;
	TArray<FName> Layers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALandscape::StaticClass(), Lands);

	// Return if there aren't
	// any landscapes in the
	// scene
	if (Lands.Num() == 0)
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1,
				15.0f,
				FColor::Red,
				TEXT("Can't find the landscape actor!"));
		return nullptr;
	}

	return Cast<ALandscape>(Lands[0]);
}

AProceduralFoliageVolume* ULandscapeHelper::GetFoliageVolume() const
{
	// Get all landscape actors from the scene
	TArray<AActor*> VolumeActors;
	TArray<FName> Layers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AProceduralFoliageVolume::StaticClass(), VolumeActors);

	// Return if there aren't
	// any landscapes in the
	// scene
	if (VolumeActors.Num() == 0)
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1,
				15.0f,
				FColor::Red,
				TEXT("Can't find the landscape actor!"));

		// TODO : Instantiate a
		//  new volume actor 
		return nullptr;
	}
	return Cast<AProceduralFoliageVolume>(VolumeActors[0]);
}

FString ULandscapeHelper::GetCurrentLevel() const
{
	UWorld* MyWorld = GetWorld();
	FString CurrentLevelName = MyWorld->GetMapName();
	
	return CurrentLevelName;
}

void ULandscapeHelper::SaveAsset(FString AssetPath)
{
	UEditorAssetLibrary::SaveAsset(AssetPath, true);
}

void ULandscapeHelper::LoadSubLevel(FString Name)
{
	FWorldBrowserModule& WorldBrowserModule = FModuleManager::GetModuleChecked<FWorldBrowserModule>("WorldBrowser");
	TSharedPtr<FLevelCollectionModel> SharedWorldModel = WorldBrowserModule.SharedWorldModel(GetWorld());

	// Filter the list of active names and
	//  select the one that matches the desired name
	const FLevelModelList& modelLevels = SharedWorldModel->GetAllLevels().FilterByPredicate(
		[Name](const TSharedPtr<FLevelModel, ESPMode::Fast>& Str){
			return Str.Get()->GetDisplayName().Equals(Name);
		});
	SharedWorldModel->LoadLevels(modelLevels);

	// get all layer info objects
	// and load them after loading
	// the level
	// ULandscapeLayerInfoObject * LayerInfoObject;
}

void ULandscapeHelper::LoadSubLevelWInfoObjects(FString Name, TArray<FString> LayerNames)
{
	LoadSubLevel(Name);

	// get all layer info objects
	// and load them after loading
	// the level
	for (int i = 0; i < LayerNames.Num(); i++)
	{
		FString LayerName = LayerNames[i];

		// create the path to the data asset
		TArray< FStringFormatArg > Args;
		const FString levelName = GetCurrentLevel();

		Args.Add(FStringFormatArg(GetLevelPath()));
		Args.Add(FStringFormatArg(levelName));
		Args.Add(FStringFormatArg(LayerName));

		// Get the path to the asset folder
		const FString AssetName = FString::Format(TEXT("{0}/{1}_sharedassets/LayerInfoObject_{2}"), Args);

		if (UEditorAssetLibrary::DoesAssetExist(AssetName))
		{
			ULandscapeLayerInfoObject * LayerInfoObject =
				Cast<ULandscapeLayerInfoObject>(UEditorAssetLibrary::LoadAsset(AssetName));		

			AssignLayer(LayerInfoObject, i);
		}
		else
		{	
			CreateLayer(LayerName, i);
		}
	}
}

void ULandscapeHelper::UnloadSubLevel(FString Name)
{
	FWorldBrowserModule& WorldBrowserModule = FModuleManager::GetModuleChecked<FWorldBrowserModule>("WorldBrowser");
	TSharedPtr<FLevelCollectionModel> SharedWorldModel = WorldBrowserModule.SharedWorldModel(GetWorld());

	// Filter the list of active names and
	//  select the one that matches the desired name
	const FLevelModelList& modelLevels = SharedWorldModel->GetAllLevels().FilterByPredicate(
		[Name](const TSharedPtr<FLevelModel, ESPMode::Fast>& Str){
			return Str.Get()->GetDisplayName().Equals(Name);
		});
	SharedWorldModel->UnloadLevels(modelLevels);
}

TArray<FString> ULandscapeHelper::GetLevelNames()
{
	TArray<FString> Names;
	FWorldBrowserModule& WorldBrowserModule = FModuleManager::GetModuleChecked<FWorldBrowserModule>("WorldBrowser");
	TSharedPtr<FLevelCollectionModel> SharedWorldModel = WorldBrowserModule.SharedWorldModel(GetWorld());

	for(TSharedPtr<FLevelModel, ESPMode::Fast> LevelModel : SharedWorldModel->GetAllLevels())
	{
		Names.Add(LevelModel->GetDisplayName());
	}
	
	return Names;
}

void ULandscapeHelper::SaveSubLevel(FString Name)
{
	FWorldBrowserModule& WorldBrowserModule = FModuleManager::GetModuleChecked<FWorldBrowserModule>("WorldBrowser");
	TSharedPtr<FLevelCollectionModel> SharedWorldModel = WorldBrowserModule.SharedWorldModel(GetWorld());

	// Filter the list of active names and
	//  select the one that matches the desired name
	const FLevelModelList& modelLevels = SharedWorldModel->GetAllLevels().FilterByPredicate(
		[Name](const TSharedPtr<FLevelModel, ESPMode::Fast>& Str){
			return Str.Get()->GetDisplayName().Equals(Name);
		});
	SharedWorldModel->SaveLevels(modelLevels);
}

FString ULandscapeHelper::GetLevelPath()
{
	UWorld* World = GetWorld();
	FString Name = World->GetPathName();
	FString LeftString, RightString;

	Name = Name.Reverse();
	Name.Split("/", &LeftString, &RightString);
	Name = RightString.Reverse();
	
	return Name;
}

FIntPoint ULandscapeHelper::GetRes()
{
	
	// Check if the landscape
	// is created
	if (!Landscape)
		Landscape = GetLandscape();

	if (!Landscape)
		return FIntPoint();

	// Get a reference for the landscape Info
	ULandscapeInfo* Info = Landscape->GetLandscapeInfo();
	FIntPoint point = Landscape->ComputeComponentCounts();

	FIntPoint NumComponents(0, 0);
	FIntPoint MaxSectionBase(0, 0);
	FIntPoint MinSectionBase(0, 0);
	
	TArray<AActor*> LandscapeProxies;
	// TArray<FName> Layers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALandscapeStreamingProxy::StaticClass(), LandscapeProxies);
	if (LandscapeProxies.Num() == 0)
		return FIntPoint();
	
	ALandscapeProxy* Proxy = Cast<ALandscapeProxy>(LandscapeProxies[0]);
	const FIntPoint Offset = Proxy->LandscapeSectionOffset;

	for (ULandscapeComponent* Component : Proxy->LandscapeComponents)
	{
		
		MaxSectionBase.X = FMath::Max(MaxSectionBase.X, Component->SectionBaseX - Offset.X);
		MaxSectionBase.Y = FMath::Max(MaxSectionBase.Y, Component->SectionBaseY - Offset.Y);

		MinSectionBase.X = FMath::Min(MinSectionBase.X, Component->SectionBaseX - Offset.X);
		MinSectionBase.Y = FMath::Min(MinSectionBase.Y, Component->SectionBaseY - Offset.Y);
		
	}
	
	NumComponents.X = ((MaxSectionBase.X - MinSectionBase.X) / Landscape->ComponentSizeQuads) + 1;
	NumComponents.Y = ((MaxSectionBase.Y - MinSectionBase.Y) / Landscape->ComponentSizeQuads) + 1;

	return NumComponents * Landscape->ComponentSizeQuads + 1;
}

TArray<FVector2D> ULandscapeHelper::GetPlantPositions()
{
	TArray<FVector2D> Positions = TArray<FVector2D>();
	
	if (!AssignFoliageVolume())
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(
				-1,15.0f, FColor::Red,
				TEXT("Failed to get foliage volume"));

		return Positions;
	}

	// TArray<FProceduralFoliageInstance> FoliageInstances;
	// FoliageComponent->FoliageSpawner->GetInstancesToSpawn(FoliageInstances);
	// if (FoliageInstances.Num() > 0)
	// {
	// 	for (int i = 0; i < FoliageInstances.Num(); i++)
	// 	{
	// 		FProceduralFoliageInstance Instance = FoliageInstances[i];
	// 		FVector location = Instance.Location;
	// 		Positions.Add(FVector2D(location.X, location.Y));
	// 	}
	// }
	
	return Positions;
}

TArray<FVector2D> ULandscapeHelper::GetBounds()
{
	AssignFoliageVolume();
	FBox Box = FoliageComponent->GetBounds();
	TArray<FVector2D> arr = TArray<FVector2D>();

	const FVector2D BottomCorner = FVector2D(Box.Min.X, Box.Min.Y);
	arr.Add(BottomCorner);

	const FVector2D TopCorner = FVector2D(Box.Max.X, Box.Max.Y);
	arr.Add(TopCorner);

	return arr;
}

void ULandscapeHelper::AssignLayer(ULandscapeLayerInfoObject * LayerInfoObject, int Index)
{
	// Check if the landscape
	// is created
	if (!Landscape)
		Landscape = GetLandscape();

	if (!Landscape)
		return;

	// Get a reference for the landscape Info
	ULandscapeInfo* LandscapeInfo = Landscape->GetLandscapeInfo();
	FLandscapeInfoLayerSettings* LayerSettings = &LandscapeInfo->Layers[Index];
	// Assign the respective values
	if (!LayerSettings->LayerInfoObj)
	{
		LayerSettings->Owner = Landscape;
		LayerSettings->LayerInfoObj = LayerInfoObject;
		LayerSettings->bValid = true;
	}
}

ULandscapeHelper::ULandscapeHelper()
{
	
}

TArray<FName> ULandscapeHelper::GetLayers()
{
	TArray<FName> Layers;
	
	// Check if the landscape
	// is created
	if (!Landscape)
		Landscape = GetLandscape();

	if (!Landscape)
		return Layers;
	
	// get all the landscape layers
	Layers =  Landscape->Layers;
	return Layers;
}

/**
 * This method creates a new layer
 * info objet but doesn't assign it
 * to the editor
 */
void ULandscapeHelper::CreateLayer(FString LayerName, int Index)	
{
	CreateLayerFromPath(GetLevelPath(), LayerName, Index);
}

void ULandscapeHelper::CreateLayerFromPath(FString Path, FString LayerName, int Index)
{
	// Check if the landscape
	// is created
	if (!Landscape)
		Landscape = GetLandscape();

	if (!Landscape)
		return;
	
	// Get the current levels name
	TArray< FStringFormatArg > Args;
	const FString levelName = GetCurrentLevel();
	Args.Add(FStringFormatArg(Path));
	Args.Add(FStringFormatArg(levelName));

	// Get the path to the asset folder
	const FString AssetName = FString::Format(TEXT("{0}/{1}_sharedassets"), Args);
	
	// This creates a new registry module
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	if (!AssetRegistryModule.Get().PathExists(AssetName))
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1,
				15.0f,
				FColor::Cyan,
				TEXT("Created new folder"));
		AssetRegistryModule.Get().AddPath(AssetName);	
	}

	// This method creates a
	// new layer info objectB
	UWorld * CurrentWorld = GetWorld();
	ULevel * Level = CurrentWorld->GetLevel(0); // TODO : Verify if the asset has been created and store it correctly
	ULandscapeLayerInfoObject* LayerInfo = Landscape->CreateLayerInfo(*(LayerName), Level);

	bool bIsDirty = LayerInfo->MarkPackageDirty();
	const FString FullPath = UEditorAssetLibrary::GetPathNameForLoadedAsset(LayerInfo);
	UEditorAssetLibrary::SaveAsset(FullPath, false);

	// This method is responsible for
	// assigning the necessary values
	AssignLayer(LayerInfo, Index);
}

void ULandscapeHelper::FillLayer(int layer)
{
}

void ULandscapeHelper::GetLandscapeLayers()
{
	// Check if the landscape
	// is created
	if (!Landscape)
		Landscape = GetLandscape();

	if (!Landscape)
		return;

	// Get all the landscapes
	TArray<FLandscapeLayer> layers = Landscape->LandscapeLayers;
	int i = 0, length = layers.Num();

	for (i = 0; i < length; i++)
	{
		FLandscapeLayer layer = layers[i];
		FString layerName = layer.Name.ToString();
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1,
				15.0f,
				FColor::Cyan,
				layerName);
	}
}

void ULandscapeHelper::Simulate()
{
	if (!AssignFoliageVolume())
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1,
				15.0f,
				FColor::Red,
				TEXT("Failed to get foliage volume"));
		return;
	}

	TArray<FDesiredFoliageInstance> FoliageInstances;
	if (FoliageComponent->GenerateProceduralContent(FoliageInstances))
	{
		if (FoliageInstances.Num() > 0)
		{
			FoliageComponent->RemoveProceduralContent(false);
			
			FFoliagePaintingGeometryFilter OverrideGeometryFilter;
			OverrideGeometryFilter.bAllowLandscape = FoliageComponent->bAllowLandscape;
			OverrideGeometryFilter.bAllowStaticMesh = FoliageComponent->bAllowStaticMesh;
			OverrideGeometryFilter.bAllowBSP = FoliageComponent->bAllowBSP;
			OverrideGeometryFilter.bAllowFoliage = FoliageComponent->bAllowFoliage;
			OverrideGeometryFilter.bAllowTranslucent = FoliageComponent->bAllowTranslucent;
			
			//FEdModeFoliage::AddInstances(FoliageComponent->GetWorld(), FoliageInstances, OverrideGeometryFilter, true);
		}
	}
}

bool ULandscapeHelper::AssignFoliageVolume()
{	
	if (!FoliageVolume)
		FoliageVolume = GetFoliageVolume();
	else
		return true;
	
	if (FoliageVolume == nullptr)
		return false;

	FoliageComponent = FoliageVolume->ProceduralComponent;
	return true;
}

void ULandscapeHelper::CreateFoliageActor()
{
}

void ULandscapeHelper::LoadLevel(FString LevelName)
{
	UEditorLevelLibrary::LoadLevel(LevelName);
}
