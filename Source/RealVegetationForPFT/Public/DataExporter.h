// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DataExporter.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class REALVEGETATIONFORPFT_API UDataExporter : public UObject
{
	struct Pixel
	{
		int X;
		int Y;
		float Value;
	};

	struct 
	{
		uint8 value;
		uint8 count;
	};
	
	GENERATED_BODY()
	
	public:
		UDataExporter();

		// Function for creating a
		//  texture based on set resolution
		void CreateTexture(int Width, int Height);
	
		// Function for creating a new texture
		UFUNCTION(BlueprintCallable)
		void CreateTexture();

		// Function for creating the final texture
		UFUNCTION(BlueprintCallable)
		void CreateDensityMapOld(
			TArray<FVector2D> Positions,
			TArray<FVector2D> Radius,
			int Width, int Height,
			FVector2D BottomLeftPosition,
			FVector2D TopRightPosition);

	
		// Function for creating the final texture
		UFUNCTION(BlueprintCallable)
		void CreateDensityMap(
			TArray<FVector2D> Positions,
			FVector2D Radius,
			int Width, int Height,
			FVector2D BottomLeftPosition,
			FVector2D TopRightPosition);
	
	private:
		FVector2D BottomLeftPosition;
		FVector2D TopRightPosition;

		int ImageWidth;
		int ImageHeight;

		float Width;
		float Height;

		Pixel GetPixel(FVector2D Vector, float Value);

		// Returns the integer size of
		//  the window being processed
		int GetWindow(float Radius);

		float GetValue(float Distance, FVector2D Radius);

		void PaintPixel(uint8 * Pixels, TArray<FVector2D>, int Diameter, FVector2D Radius, int X, int Y);
		
		// Returns the point of an object in refer
		FIntPoint GetIndex(FVector2D position);

		// for a specific point it returns
		//  
		TArray<Pixel> GetPoint(FVector2D Positions, FVector2D Radius);

		// Returns a list of points
		//  and their values based
		//  on the provided radius
		TArray<Pixel> GetAllPoints(TArray<FVector2D> Positions, TArray<FVector2D> Radius);

		void CreateTexture(int Width, int Height, uint8 * Pixels);

		uint8 * CreateDensityArrayOld(TArray<FVector2D> Positions, TArray<FVector2D> Radius);

		uint8* CreateDensityArray(TArray<FVector2D> Positions, FVector2D Radius);
};
