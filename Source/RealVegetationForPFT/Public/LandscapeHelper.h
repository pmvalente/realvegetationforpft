// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// #include "Landscape.h"


#include "CoreMinimal.h"
#include "InputCoreTypes.h"
#include "InstancedFoliage.h"
#include "UnrealWidget.h"
#include "EdMode.h"
#include "Widgets/Views/SHeaderRow.h"

#include "UObject/Object.h"
#include "Kismet/GameplayStatics.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "EditorAssetLibrary.h"

#include "ProceduralFoliageComponent.h"
#include "ProceduralFoliageVolume.h"
#include "Landscape/Classes/Landscape.h"
#include "LandscapeHelper.generated.h"


/**
 * This class has a set of
 * functions that helps you
 * deal with the ue4 landscape
 * edito
 */
UCLASS(Blueprintable, BlueprintType)
class REALVEGETATIONFORPFT_API ULandscapeHelper : public UObject
{
	GENERATED_BODY()
	
	public:
		ULandscapeHelper();

		ALandscape * Landscape;

		UProceduralFoliageComponent* FoliageComponent;

		AProceduralFoliageVolume* FoliageVolume;

		UFUNCTION(BlueprintCallable)
		TArray<FName> GetLayers();
		
		UFUNCTION(BlueprintCallable)
		void CreateLayer(FString LayerName, int Index);
		
		UFUNCTION(BlueprintCallable)
		void CreateLayerFromPath(FString Path, FString LayerName, int Index);

		UFUNCTION(BlueprintCallable)
		void FillLayer(int layer);

		UFUNCTION(BlueprintCallable)
		void GetLandscapeLayers();

		UFUNCTION(BlueprintCallable)
		void Simulate();

		UFUNCTION(BlueprintCallable)
		bool AssignFoliageVolume();

		UFUNCTION(BlueprintCallable)
		void CreateFoliageActor();

		UFUNCTION(BlueprintCallable)
		void LoadLevel(FString LevelName);

		UFUNCTION(BlueprintCallable)
		static void SaveAsset(FString AssetPath);

		UFUNCTION(BlueprintCallable)
		void LoadSubLevel(FString Name);

		UFUNCTION(BlueprintCallable)
		void LoadSubLevelWInfoObjects(FString Name, TArray<FString> LayerNames);

		UFUNCTION(BlueprintCallable)
		void UnloadSubLevel(FString Name);

		UFUNCTION(BlueprintCallable)
		TArray<FString> GetLevelNames();

		UFUNCTION(BlueprintCallable)
		void SaveSubLevel(FString Name);
		
		UFUNCTION(BlueprintCallable)
		FString GetLevelPath();

		UFUNCTION(BlueprintCallable)
		FIntPoint GetRes();

		UFUNCTION(BlueprintCallable)
		TArray<FVector2D> GetPlantPositions();

		UFUNCTION(BlueprintCallable)
		TArray<FVector2D> GetBounds();
	
	private:
		ALandscape* GetLandscape() const;

		AProceduralFoliageVolume* GetFoliageVolume() const;

		FString GetCurrentLevel() const;

		void AssignLayer(ULandscapeLayerInfoObject * LayerInfoObject, int Index);
};
