# Read Me

## Introduction

This plugin creates an interface for the procedural foliage tool that aids user’s in creating vegetation for a world based on real world data.

---

## Structure

The system is divided into two logic parts, the component that handles the processing of real data, this is mostly done through python. the second component handles the automation process of the generation and it was mostly developed with C++, python and blueprints.

Regarding the overall structure of the project, the python folder contains all the scripts that are going to be used in the process of transforming and getting the real world data. Here we can also find all the scripts that are used in the automation process.

The `GrassTypes` folder contains all the grass models that we can use to put on the environment we are trying to represent. To add new grass types we need to also add the grass types to `GrassType` settings data object that is located in the `Widgets/DataAssets` folder. This folder is important because it contains all the temporary configurations on the editor side.

---

## Installation

To install this project you have to have a version of the unreal engine 4 that is higher than the version 4.26, other than this we need to have the anaconda python library installed. The installation process is done in two steps, one is done without unreal the other is done inside the project itself.

To start off you have to download the latest version of the project and save the plugin inside the plugins folder.

With this done you need to setup the necessary python libraries, to do so you can to use conda, in the project there is a `.yml` file named `RealVegetationForPFTEnv.yml`, it contains the python environment that is being used, this file is stored in the following path `/realvegetationforpft/Content/Pyhton/Environment`, to install this environment you have run the the following command in the `conda env create -f RealVegetationForPFTEnv.yml` while on the folder of the `RealVegetationForPFTEnv.yml` file.

With this done step done you have to set up the environment variables, to do this you need to edit the `pcg_config.ini` file, stored in the the `/realvegetationforpft/Content/Pyhton` folder. To be more precise you need to change the `[SYSTEM_PATHS]` field. By changing it the following way:

```jsx
[SYSTEM_PATHS]
nb_of_paths=2
system_path_1 = PATH_TO_ENVIRONMENT\\Lib\\site-packages
system_path_2 = PATH_TO_ENVIRONMENT\\Lib\\site-packages\\pip\\_vendor
```

Here **PATH_TO_ENVIRONMENT** corresponds to the full path to you environment, this can usually be found in your anaconda installation folder under the `envs` folder. Alternatively you can setup the python dependency variables in the python editor by going to python plugin tab and adding the same two paths that we saw before manually.

After setting up the environment variable you have to also setup the temporary directory where you are going to store the images thata are going to be imported to the engine. To do so you have to edit the field **TEMPORARY_FOLDER** in the `pcg_config.ini` file.

```jsx
[FILE_NAMES]
table_name = test_table_1.txt
image_path = test_image_1.png
image_save_path = test_image_2.jpg
temp_folder = TEMPORARY_FOLDER
```

Then you also have to unzip the COS file located in the `realvegetationforpft/Content/Pyhton/Data` folder, so you have a local version of COS, and change the location of the default path in the `shapefile_processor.py` script.

After this last step you are now able to start generating vegetation. So a short list of what you have to do to install the plugin:

- Install a compatible version of the unreal 4 engine (4.26 or above), and install the python plugin;
- Download and extract the systems zip to your project’s plugin folder;
- Create the python environment that contains all the necessary libraries;
- Setup the configuration file so you can use the necessary libraries;
- Change the path to the temporary folder;
- unzip the COS zip;

---

## How to use

You can run this system by using two different methods:

- Running the script manually in the python console;
- Using the developed interface;

<aside>
💡 **NOTE**: While using the user interface it’s important that every field is correctly filled to prevent possible generation problems and errors.

</aside>

To run the script manually you need to write the following command:

```python
import AccessScript as ac

ac.process_data()
```

Running this command this way will use the values that are set up in the pcg_config.ini file, you can edit these values in order to switch out the configuration that you are using, here are what each value means:

```python
[DOWNLOAD]
bound_x_min=-946914.4277606456
bound_y_min=4821995.293980075
bound_x_max=-811315.1395826909
bound_y_max=4886202.397739647
img_size_x=1280
img_size_y=606
```

The download section allows you to define the boundaries that define the area that is being represented and the resolution of the images that are going to be used for generating the multiple density maps.

```python
[LAND_COVER]
nb_of_layers=2
layer_0_name=arvores_resistentes
nb_of_merged_layers_0=9
layer_id_00=5.1.1.3
layer_id_01=5.1.1.4
layer_id_02=5.1.1.1
layer_id_03=5.1.1.2
layer_id_04=5.1.1.7
layer_id_05=4.1.1.1
layer_id_06=4.1.1.2
layer_id_07=4.1.1.3
layer_id_08=4.1.1.6
layer_1_name=arvores_de_risco
nb_of_merged_layers_1=8
layer_id_10=5.1.2.1
layer_id_11=5.1.2.2
layer_id_12=5.1.2.3
layer_id_13=4.1.1.4
layer_id_14=5.1.1.5
layer_id_15=5.1.1.6
layer_id_16=4.1.1.5
layer_id_17=4.1.1.7
```

The land cover sections allows you to define the composition of every layer that is being represented in the world. To define this you have to correctly define the number of layers that are being represented and the classes that belong to each layer.

```python
[TILE_SETTINGS]
width=2
height=2
```

The last important setting is the tile importation setting, this setting cannot be changed and is automatically assigned during generation.

The importation interface although being very basic it can also be used to quickly set up the importation settings, here you can define the bounds that of the represented area in a single string composed of `x_min,x_max,y_min,y_max`, here you can also define the image resolution for the processed image by setting the following values `width,height`. 

These two settings will allow you to create the vegetation of an area using the default vegetation configuration, you can also edit this configuration by changing it in the interface. Here, for every class that you want to represent you have to added it to the menu a assign every class that you can observe in the layer being represented