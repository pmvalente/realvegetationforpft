Texture2D<float4> Texture;
sampler TextureSampler;

float Width : register(C0);
float Height : register(C1);

// noperspective float2 TEXCOORD;
static const float2 size = {2.0,0.0};
static const float3 off = {-1.0,0.0,1.0};
static const float2 nTex = {Width, Height};

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    float4 color = Texture.Sample(TextureSampler, uv);
    // float y = 0.299 * color.x + 0.587 * color.y + 1.114 * color.z;
    // float green = color.y;

    float2 offxy = {off.x/nTex.x , off.y/nTex.y};
    float2 offzy = {off.z/nTex.x , off.y/nTex.y};
    float2 offyx = {off.y/nTex.x , off.x/nTex.y};
    float2 offyz = {off.y/nTex.x , off.z/nTex.y};

    float s11 = color.x;
    float s01 = Texture.Sample(TextureSampler, uv.xy + offxy).x;
    float s21 = Texture.Sample(TextureSampler, uv.xy + offzy).x;
    float s10 = Texture.Sample(TextureSampler, uv.xy + offyx).x;
    float s12 = Texture.Sample(TextureSampler, uv.xy + offyz).x;
    float3 va = {size.x, size.y, s21-s01};
    float3 vb = {size.y, size.x, s12-s10};
    va = normalize(va);
    vb = normalize(vb);
    float4 bump = {(cross(va,vb)) / 2 + 0.5, 1.0};
    return bump;
}