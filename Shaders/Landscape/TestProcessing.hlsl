Texture2D<float4> Texture;
sampler TextureSampler;

Texture2D<float4> RoadMask;
sampler RoadMaskSampler;

float _MinDistanceToCell;
float _Offset;
float _NoiseStrength;
float _CellSize;
float _NSize;


#include "noiseSimplex.cginc"

float fbm (float2 x, float H)
{
    float G = exp2(-H);
    float f = 1.0;
    float a = 1.0;
    float t = 0.0;
    for( int i=0; i<8; i++ )
    {
        t += a*(snoise(f*x) + 1)/2;
        f *= 2.0;
        a *= G;
    }
    return t;
}

float random(float2 uv)
{
    return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453123);
}

float2 voronoiNoise(float2 value)
{
    const float2 baseCell = floor(value);

    float min_dist_to_cell = _MinDistanceToCell;
    float2 closest_cell;
    [unroll]
    for (int x = -1; x < 2; x++)
    {
        [unroll]
        for (int y = -1; y < 2; y++)
        {
            const float2 cell = baseCell + float2(x, y);
            const float2 rand = float2(random(cell), random(cell + float2(_Offset, 0))) * _NoiseStrength;
            const float2 cell_position = cell + rand;

            const float2 toCell = cell_position - value;

            const float distToCell = length(toCell);
            const int t = distToCell <= min_dist_to_cell;

            min_dist_to_cell = t * distToCell + (1 - t) * min_dist_to_cell;
            closest_cell = t * cell + (1 - t) * float2(0, 0);
        }
    }
    float rand_2 = random(closest_cell * _Offset);
    return float2(min_dist_to_cell, rand_2);
}

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    float4 color = Texture.Sample(TextureSampler, uv);
    float4 road_mask = RoadMask.Sample(RoadMaskSampler, uv);
    // road_mask = road_mask * road_mask.w;

    // float y = 0.299 * color.x + 0.587 * color.y + 1.114 * color.z;
    // float green = color.y;

    float scaled_uv = uv / _NSize;;

    float2 q = float2( fbm( scaled_uv + float2(0.0,0.0), 1 ),
                fbm( uv + float2(5.2,1.3), 1 ) );

    float2 r = float2( fbm( uv + 4.0*q + float2(1.7,9.2), 1 ),
                fbm( uv + 4.0*q + float2(8.3,2.8), 1 ) );
                
    const float2 value = uv / _CellSize;

    float final_noise = (fbm( uv + 4.0*r, 10 ) * 1 * voronoiNoise(value)) + 0.7;

    return (1 - road_mask) * (final_noise * color) + road_mask * (color);// color.y; // lerp(0, y, color.y);
}