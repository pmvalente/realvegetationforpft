Texture2D<float4> Texture;
sampler TextureSampler;

float Width : register(C0);
float Height : register(C1);

// noperspective float2 TEXCOORD;
static const float2 size = {2.0,0.0};
static const float3 off = {-1.0,0.0,1.0};
static const float2 nTex = {Width, Height};

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    float4 color = Texture.Sample(TextureSampler, uv);
    color = color * 10;
    color = saturate(color);

    return color;
}