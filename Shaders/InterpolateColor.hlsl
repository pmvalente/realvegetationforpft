Texture2D<float4> texture;
sampler textureSampler;

Texture2D<float4> colorRamp;
sampler colorRampSampler;

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    float4 color = texture.Sample(textureSampler, uv);
    float y = 0.299 * color.x + 0.587 * color.y + 1.114 * color.z;
    float4 col = colorRamp.Sample(colorRampSampler, float2(y * 0.3, 0.5));

    return col;
}