Texture2D<float4> Texture;
sampler TextureSampler;
float width = 400;
float height = 400;

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    int i = 0;
    // uint width, height;
    // Texture.GetDimensions(width, height);

    // Calcular as coordenadas necessárias
    float2 res = float2(width, height);
    float2 coords [16];
    coords[0] = floor(res * uv);
    
    coords[1] = (coords[0] + float2(-1, -1)) / res;
    coords[2] = (coords[0] + float2(0, -1)) / res;
    coords[3] = (coords[0] + float2(1, -1)) / res;
    coords[4] = (coords[0] + float2(2, -1)) / res;
    coords[5] = (coords[0] + float2(-1, 0)) / res;
    coords[6] = (coords[0] + float2(1, 0)) / res;
    coords[7] = (coords[0] + float2(2, 0)) / res;
    coords[8] = (coords[0] + float2(-1, 1)) / res;
    coords[9] = (coords[0] + float2(0, 1)) / res;
    coords[10] = (coords[0] + float2(1, 1)) / res;
    coords[11] = (coords[0] + float2(2, 1)) / res;
    coords[12] = (coords[0] + float2(-1, 2)) / res;
    coords[13] = (coords[0] + float2(0, 2)) / res;
    coords[14] = (coords[0] + float2(1, 2)) / res;
    coords[15] = (coords[0] + float2(2, 2)) / res;
    coords[0] = coords[0] / res;

    float4 values [16];

    [unroll]
    for (i=0; i<16; i++)
    {
        values[i] = Texture.Sample(TextureSampler, coords[i]);
    }

    // int v00=1, v01=5, v02=8, v03=12;
    // int v10=2, v11=0, v12=9, v13=13;
    // int v20=3, v21=6, v22=10, v23=14;
    // int v30=4, v31=7, v32=11, v33=15;

    float4 a00, a01, a02, a03;
	float4 a10, a11, a12, a13;
	float4 a20, a21, a22, a23;
	float4 a30, a31, a32, a33;

    a00 = values[0];
    a01 = -.5*values[2] + .5*values[9];
    a02 = values[2] - 2.5*values[0] + 2*values[9] - .5*values[13];
    a03 = -.5*values[2] + 1.5*values[0] - 1.5*values[9] + .5*values[13];
    
    a10 = -.5*values[5] + .5*values[6];
    a11 = .25*values[1] - .25*values[8] - .25*values[3] + .25*values[10];
    a12 = -.5*values[1] + 1.25*values[5] - values[8] + .25*values[12] + .5*values[3] - 1.25*values[6] + values[10] - .25*values[14];
    a13 = .25*values[1] - .75*values[5] + .75*values[8] - .25*values[12] - .25*values[3] + .75*values[6] - .75*values[10] + .25*values[14];
    
    a20 = values[5] - 2.5*values[0] + 2*values[6] - .5*values[7];
    a21 = -.5*values[1] + .5*values[8] + 1.25*values[2] - 1.25*values[9] - values[3] + values[10] + .25*values[4] - .25*values[11];
    a22 = values[1] - 2.5*values[5] + 2*values[8] - .5*values[12] - 2.5*values[2] + 6.25*values[0] - 5*values[9] + 1.25*values[13] + 2*values[3] - 5*values[6] + 4*values[10] - values[14] - .5*values[4] + 1.25*values[7] - values[11] + .25*values[15];
    a23 = -.5*values[1] + 1.5*values[5] - 1.5*values[8] + .5*values[12] + 1.25*values[2] - 3.75*values[0] + 3.75*values[9] - 1.25*values[13] - values[3] + 3*values[6] - 3*values[10] + values[14] + .25*values[4] - .75*values[7] + .75*values[11] - .25*values[15];
    
    a30 = -.5*values[5] + 1.5*values[0] - 1.5*values[6] + .5*values[7];
    a31 = .25*values[1] - .25*values[8] - .75*values[2] + .75*values[9] + .75*values[3] - .75*values[10] - .25*values[4] + .25*values[11];
    a32 = -.5*values[1] + 1.25*values[5] - values[8] + .25*values[12] + 1.5*values[2] - 3.75*values[0] + 3*values[9] - .75*values[13] - 1.5*values[3] + 3.75*values[6] - 3*values[10] + .75*values[14] + .5*values[4] - 1.25*values[7] + values[11] - .25*values[15];
    a33 = .25*values[1] - .75*values[5] + .75*values[8] - .25*values[12] - .75*values[2] + 2.25*values[0] - 2.25*values[9] + .75*values[13] + .75*values[3] - 2.25*values[6] + 2.25*values[10] - .75*values[14] - .25*values[4] + .75*values[7] - .75*values[11] + .25*values[15];

    float2 relative_coords = (uv - coords[0]) * res;
    float x = relative_coords.x;// (uv.x - coords[0].x) * res;
    float y = relative_coords.y;// (uv.y - coords[0].y) * res;
    
    float x2 = x * x;
    float x3 = x2 * x;

    float y2 = y * y;
    float y3 = y2 * y;

    float4 color = (a00 + a01 * y + a02 * y2 + a03 * y3) +
		       (a10 + a11 * y + a12 * y2 + a13 * y3) * x +
		       (a20 + a21 * y + a22 * y2 + a23 * y3) * x2 +
		       (a30 + a31 * y + a32 * y2 + a33 * y3) * x3;

    float4 Texture_col = Texture.Sample(TextureSampler, uv);
    float2 coord = coords[6]; 
    return color;// float4(coord.x, coord.y, 0, 1);// color;
}