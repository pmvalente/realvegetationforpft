Texture2D<float4> texture;
sampler textureSampler;

float TileWidth = 2;
float TileHeight = 2;

float TilePosX = 0;
float TilePosY = 0;

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    float2 tile_res = float2(TileWidth, TileHeight);
    float2 tile_pos = float2(TilePosX, TilePosY);
    float2 inverse_res = 1 / tile_res;
    float2 scaled_uv = (uv * inverse_res) + tile_pos * inverse_res;

    float4 texture_col = texture.Sample(textureSampler, scaled_uv);
    return texture_col; float4(scaled_uv.x, scaled_uv.y, 0, 1);
}