Texture2D<float4> texture;
sampler textureSampler;

// SamplerState textureSampler
// {
//     MinFilter = None; //Controls sampling. None, Linear, or Point.
//     MagFilter = None; //Controls sampling. None, Linear, or Point.
//     MipFilter = None; //Controls how the mips are generated. None, Linear, or Point.
// };


float width = 400;
float height = 400;

float4 main( float2 uv : TEXCOORD0) : SV_Target0
{
    int i = 0;
    
    // uint width, height;
    // texture.GetDimensions(width, height);

    // calcular as coordenadas da textura 
    // que vamos usar no upsampling
    
    float2 coords[4];
    float2 res = float2(width, height) * 1.;

    // TODO: Mudar isto para uma forma mais elegante
    coords[0] = floor(res * uv);
    coords[1] = (coords[0] + float2(0, 1)) / res;
    coords[2] = (coords[0] + float2(1, 0)) / res;
    coords[3] = (coords[0] + float2(1, 1)) / res;
    coords[0] = coords[0] / res;

    float2 relative_coords = (uv - coords[0]) * res;

    // fazer a interpolação linear
    float4 values [4];
    float a = relative_coords.x, b = relative_coords.y;

    [unroll]
    for (i=0; i<4; i++)
    {
        values[i] = texture.Sample(textureSampler, coords[i]);
    }

    float4 color = (1 - a) * (1 - b) * values[0] + 
        (1 - a) * b * values[1] + 
        a * (1 - b) * values[2] + 
        a * b * values[3];
    
    float4 texture_col = texture.Sample(textureSampler, uv);
    return color;
}