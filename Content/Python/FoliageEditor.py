import unreal
import Classes.FoliageHelper as fh
import unreal_importer as ui

from unreal_importer import *


def create_layers(layers, vegetation_types, vegetation_meshes, spawner_name='Default_Spawner', target_folder='/Game'):
    spawner_folder = f'{target_folder}/Spawner'
    foliage_type_folder = f'{target_folder}/Foliage_Types'

    foliage_helper = fh.FoliageHelper(spawner_name=spawner_name, spawner_path=spawner_folder)
    targets = zip(layers, vegetation_types, vegetation_meshes)

    for target in targets:
        layer = target[0]
        vegetation_type = target[1]
        mesh = target[2]

        # create the new foliage type
        foliage_helper.create_layer(layer, foliage_type_folder)

        # set vegetation type
        foliage_helper.set_layer_values(layer, vegetation_type)

        # assign the foliage mesh
        foliage_helper.set_layer_mesh(layer, mesh)

    foliage_helper.set_spawner_values(layers)
    return foliage_helper


def create_layers_1(layers, spawner_name='Default_Spawner', target_folder='/Game', foliage_data=None):
    spawner_folder = f'{target_folder}/Spawner'
    foliage_type_folder = f'{target_folder}/Foliage_Types'

    foliage_helper = fh.FoliageHelper(spawner_name=spawner_name, spawner_path=spawner_folder)
    final_layers = []

    for layer in layers:
        # print(f'AAAAAAAAAAAAAAAAAAAAAAAH DATAT: {vegetation_data}')
        vegetation_data = foliage_data[layer]
        asset_path = vegetation_data['asset_path']

        if asset_path:
            mesh = import_asset(asset_path)

            # create the new foliage type
            foliage_helper.create_layer(layer, foliage_type_folder)

            # assign the foliage mesh
            foliage_helper.set_layer_mesh(layer, mesh, vegetation_data)
            final_layers.append(layer)

    foliage_helper.set_spawner_values(final_layers)
    return foliage_helper


def create_layers_2(layers, spawner_name='Default_Spawner', target_folder='/Game'):
    spawner_folder = f'{target_folder}/Spawner'
    foliage_type_folder = f'{target_folder}/Foliage_Types'
    foliage_helper = fh.FoliageHelper(spawner_name=spawner_name, spawner_path=spawner_folder)

    for layer in layers:
        # create the new foliage type
        foliage_helper.create_layer(layer, foliage_type_folder)

    foliage_helper.set_spawner_values(layers)
    return foliage_helper


def import_mesh(mesh_path=''):
    return ui.import_asset(mesh_path)
