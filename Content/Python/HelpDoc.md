# Help Doc

Full url : "https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_TreeCoverDensity_2018/ImageServer/exportImage?bbox=-3551136.8122000005%2C3128825.7097000033%2C5142393.1877999995%2C1.1831355709700003E7&bboxSR=&size=&imageSR=&time=&format=jpgpng&pixelType=U8&noData=&noDataInterpretation=esriNoDataMatchAny&interpolation=+RSP_BilinearInterpolation&compression=&compressionQuality=&bandIds=&sliceId=&mosaicRule=&renderingRule=&adjustAspectRatio=true&lercVersion=1&compressionTolerance=&f=image"

Query parameters : 

- ***bbox***: *bounding box*, ou seja os limites da imagem que está a ser renderizada.

- ***size***: Resolução desejada pela imagem.

- ***imageSR***: Referência espacial da imagem.

- ***format***: Formato desejado para obter a imagem que está a ser acedida.

- ***pixelType***: Tipo de pixel que podemos encontrar na imagem.

- ***noDataInterpretation***: ??? No Data ??? Não sei ao certo o que é.

- ***interpolation***: O tipo de filtragem que queremos aplicar às imagens que etamos a tentar obter.

- ***compression***: O tipo de compressão que queremos ver num determinado ficheiro.

- ***compressionQuality***: Qualidade da compressão que queremos obter.

- ***bandIds***: ??? Id das bandas representadas na imagem ???.

- ***sliceId***: ??? Identificador da secção que estamos a tentar obter ???.

- ***mosaicRule***: ???

- ***renderingRule***: ???

- ***adjustAspectRatio***: Eles no site perguntam se queres pixeis quadrados...

- ***lercVersion***: ???

- ***compressionTolerance***: Parametro de tolerancia da compressão.

- ***f***: formato do output

---

## Pedido realizado na zona de portugal

**f**: image

**bbox**: -2069054.2011505328,4289111.958392825,100534.40969533351,5316425.61854532

**imageSR**: 102100

**bboxSR**: 102100

**size**: 887,420