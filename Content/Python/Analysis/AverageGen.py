import numpy as np
from sklearn.model_selection import cross_val_predict
from sklearn import linear_model
import matplotlib.pyplot as plt

value_1 = [
    0.469803,
    0.69835,
    0.642022,
    0.996596,
    0.92225
]

value_2 = [
    [0.16945, 0.05228],
    [0.00390, 0.00082],
    [0.10532, 0.031296],
    [0.0482501, 0.013504],
    [0.38250, 0.0606],

    [0.0355692, 0.0125839],
    [0.03556, 0.020925],
    [0.0355692, 0.02114],
    [0.301134, 0.159563],
    [0.01045, 0.00274],

    [0.052277, 0.0135617],
    [0.022041, 0.005191],
    [0.199706, 0.057573],
    [0.05604, 0.0247209],
    [0.031808, 0.011041],

    [0.0326137, 0.0570986],
    [0.006713, 0.002073],
    [0.0520731, 0.017549],
    [0.017658, 0.008601],
    [0.045520, 0.014042],

    [0.6306021, 0.2309264],
    [0.1309925, 0.0664942781],
    [0.130992, 0.086045],
    [0.0023424, 0.0004926],
    [0.027654, 0.010434]
]

value_3 = [
    0.0317,
    0.04339038,
    0.02414872,
    0.02987272,
    0.06887846
]

value_4 = [
    0.7737692,
    0.7870064,
    0.8599944,
    0.900400,
    0.7458042
]


def avg_value(value):
    return np.mean(value)


def estimate_coef(x, y):
    # number of observations/points
    n = np.size(x)

    # mean of x and y vector
    m_x = np.mean(x)
    m_y = np.mean(y)

    # calculating cross-deviation and deviation about x
    SS_xy = np.sum(y * x) - n * m_y * m_x
    SS_xx = np.sum(x * x) - n * m_x * m_x

    # calculating regression coefficients
    b_1 = SS_xy / SS_xx
    b_0 = m_y - b_1 * m_x

    return b_0, b_1


def plot_regression_line(x, y, b):
    # plotting the actual points as scatter plot
    plt.scatter(x, y, color="m",
                marker="o", s=30)

    # predicted response vector
    y_pred = b[0] + b[1] * x

    # plotting the regression line
    plt.plot(x, y_pred, color="g")

    # putting labels
    plt.xlabel('Valor médio da imagem')
    plt.ylabel('Erro quadrático médio')

    # function to show plot
    plt.show()


def liner_regression(values):
    v = np.array(values)
    x = v[:, 0]
    y = v[:, 1]

    # estimating coefficients
    b = estimate_coef(x, y)
    print("Estimated coefficients:\nb_0 = {}  \
          \nb_1 = {}".format(b[0], b[1]))

    # plotting regression line
    plot_regression_line(x, y, b)


# def liner_regression(values):
#     v = np.array(values)
#     x = v[:, 1]
#     y = v[:, 0]
#
#     lr = linear_model.LinearRegression()
#     predicted = cross_val_predict(lr, x, y, cv=10)
#     fig, ax = plt.subplots()
#
#     ax.scatter(y, predicted, edgecolors=(0, 0, 0))
#     ax.plot([y.min(), y.max()], [y.min(), y.max()], "k--", lw=4)
#
#     ax.set_xlabel("Measured")
#     ax.set_ylabel("Predicted")
#
#     plt.show()
#     return 0

def plot_image(values, s, ymin, ymax):
    plt.plot([50, 100, 150, 200, 250], values)
    plt.xlabel('Tamanho da zona representada')
    plt.ylabel(s)

    ax = plt.gca()
    ax.set_ylim([ymin, ymax])
    plt.show()


if __name__ == '__main__':
    print(avg_value(value_1))
    liner_regression(value_2)
    plot_image(value_3, 'Erro quadrático médio', 0, 1)
    plot_image(value_4, 'Valor SSIM', -1, 1)
