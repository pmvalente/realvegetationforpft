import sys
import os
import time
import datetime
from datetime import datetime


def save_time():

    # get the time value
    time_str = f'{time.time()}'
    with open("time_values.txt", "a") as f:
        f.write(f'{time_str}, ')
        f.close()

    return 0


def clean_time_table():
    open('time_values.txt', 'w').close()
    return 0




def get_time():
    T = []
    with open("time_values.txt", "r") as f:
        for line in f.readlines():
            f_list = [float(i) for i in line.split(",") if i.strip()]

    time_elapsed = (f_list[len(f_list)-1] - f_list[0]) * 1000
    # print(time_elapsed)
    return time_elapsed, f_list


if __name__ == '__main__':
    # process_data()
    save_time()
    # start = time.time()
    #
    time.sleep(10)  # or do something more productive
    save_time()
    value, _ = get_time()
    clean_time_table()
    print(value)

    # done = time.time()
    # elapsed = done - start
    # print(elapsed)
