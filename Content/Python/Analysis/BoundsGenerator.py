import numpy as np

min_x = -946914.4277606456
min_y = 4821995.293980075
max_x = -811315.1395826909
max_y = 4886202.397739647

length_x = 1000
length_y = 1000

# -964603.480618407,4499245.210495202,-674754.2693610331,5069159.693389447

if __name__ == '__main__':
    f_x = np.random.uniform(0.0, 1.0)
    f_y = np.random.uniform(0.0, 1.0)

    pos_x = min_x + (max_x - min_x) * f_x
    pos_y = min_y + (max_y - min_y) * f_y

    print(f'{pos_x},{pos_y},{pos_x+length_x},{pos_y+length_y}')
