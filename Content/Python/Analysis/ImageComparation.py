# from unreal_importer import *
import numpy as np
import cv2
import math

from PIL import Image
import sys
import time

import errno
import base64
import skimage.viewer
from skimage.metrics import structural_similarity as ssim

from skimage import io
from skimage.color import rgb2hsv
from skimage import io
from skimage.color import rgb2hsv


# Interpolation kernel
def u(s, a):
    if (abs(s) >= 0) & (abs(s) <= 1):
        return (a + 2) * (abs(s) ** 3) - (a + 3) * (abs(s) ** 2) + 1
    elif (abs(s) > 1) & (abs(s) <= 2):
        return a * (abs(s) ** 3) - (5 * a) * (abs(s) ** 2) + (8 * a) * abs(s) - 4 * a
    return 0


# Padding
def padding(img, H, W, C):
    zimg = np.zeros((H + 4, W + 4, C))
    zimg[2:H + 2, 2:W + 2, :C] = img

    # Pad the first/last two col and row
    zimg[2:H + 2, 0:2, :C] = img[:, 0:1, :C]
    zimg[H + 2:H + 4, 2:W + 2, :] = img[H - 1:H, :, :]
    zimg[2:H + 2, W + 2:W + 4, :] = img[:, W - 1:W, :]
    zimg[0:2, 2:W + 2, :C] = img[0:1, :, :C]

    # Pad the missing eight points
    zimg[0:2, 0:2, :C] = img[0, 0, :C]
    zimg[H + 2:H + 4, 0:2, :C] = img[H - 1, 0, :C]
    zimg[H + 2:H + 4, W + 2:W + 4, :C] = img[H - 1, W - 1, :C]
    zimg[0:2, W + 2:W + 4, :C] = img[0, W - 1, :C]
    return zimg


# Bicubic operation
def bicubic(img, ratio, a):
    # Get image size
    H, W, C = img.shape

    # Here H = Height, W = weight,
    # C = Number of channels if the
    # image is coloured.
    img = padding(img, H, W, C)

    # Create new image
    dH = math.floor(H * ratio)
    dW = math.floor(W * ratio)

    # Converting into matrix
    dst = np.zeros((dH, dW, C))
    # np.zeroes generates a matrix
    # consisting only of zeroes
    # Here we initialize our answer
    # (dst) as zero

    h = 1 / ratio

    print('Start bicubic interpolation')
    print('It will take a little while...')
    inc = 0

    for c in range(C):
        for j in range(dH):
            for i in range(dW):
                # Getting the coordinates of the
                # nearby values
                x, y = i * h + 2, j * h + 2

                x1 = 1 + x - math.floor(x)
                x2 = x - math.floor(x)
                x3 = math.floor(x) + 1 - x
                x4 = math.floor(x) + 2 - x

                y1 = 1 + y - math.floor(y)
                y2 = y - math.floor(y)
                y3 = math.floor(y) + 1 - y
                y4 = math.floor(y) + 2 - y

                # Considering all nearby 16 values
                mat_l = np.matrix([[u(x1, a), u(x2, a), u(x3, a), u(x4, a)]])
                mat_m = np.matrix([[img[int(y - y1), int(x - x1), c],
                                    img[int(y - y2), int(x - x1), c],
                                    img[int(y + y3), int(x - x1), c],
                                    img[int(y + y4), int(x - x1), c]],
                                   [img[int(y - y1), int(x - x2), c],
                                    img[int(y - y2), int(x - x2), c],
                                    img[int(y + y3), int(x - x2), c],
                                    img[int(y + y4), int(x - x2), c]],
                                   [img[int(y - y1), int(x + x3), c],
                                    img[int(y - y2), int(x + x3), c],
                                    img[int(y + y3), int(x + x3), c],
                                    img[int(y + y4), int(x + x3), c]],
                                   [img[int(y - y1), int(x + x4), c],
                                    img[int(y - y2), int(x + x4), c],
                                    img[int(y + y3), int(x + x4), c],
                                    img[int(y + y4), int(x + x4), c]]])
                mat_r = np.matrix(
                    [[u(y1, a)], [u(y2, a)], [u(y3, a)], [u(y4, a)]])

                # Here the dot function is used to get
                # the dot product of 2 matrices
                dst[j, i, c] = np.dot(np.dot(mat_l, mat_m), mat_r)

    # If there is an error message, it
    # directly goes to stderr
    sys.stderr.write('\n')

    # Flushing the buffer
    sys.stderr.flush()
    return dst


def sim_score(image_path_1, image_path_2):
    return 0


def hog_distance(image_path_1, image_path_2):
    return 0


def outline_distance(image_path_1, image_path_2):
    return 0


def mean_distance(image_a, image_b):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((image_a.astype("float") - image_b.astype("float")) ** 2)
    err /= float(image_a.shape[0] * image_a.shape[1])

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err


def compare_images(image_path_1, image_path_2):

    sim = mean_distance(image_path_1, image_path_2)
    hog_dist = mean_distance(image_path_1, image_path_2)

    outline = mean_distance(image_path_1, image_path_2)
    mean = mean_distance(image_path_1, image_path_2)

    return sim, hog_dist, outline, mean


def process_image(image_path, return_path='', window_size=10):
    # Given a certain image
    img = io.imread(image_path)

    nb_of_pixels = window_size ** 2
    nb_of_it_x = img.shape[1] // window_size
    nb_of_it_y = img.shape[0] // window_size
    arr = []

    for y in range(0, nb_of_it_y):
        for x in range(0, nb_of_it_x):
            start_pos_x = x * window_size
            start_pos_y = y * window_size

            section = img[
                      start_pos_y:start_pos_y + window_size,
                      start_pos_x:start_pos_x + window_size
                      ]
            red_channels = section[:, :, 0]
            value = red_channels.sum() // nb_of_pixels
            arr.append(value)
            arr.append(value)
            arr.append(value)

    arr = np.array(arr)
    arr = arr.reshape(nb_of_it_y, nb_of_it_x, 3)
    io.imsave(return_path, arr)

    return return_path


def process_images(
        image_reference, image_path_1,
        image_path_2, image_path_3,
        window_size=10):

    #    export_texture(image_reference, image_path_1)
    process_image(image_path_1, image_path_2)

    # Get the distance values
    sim, hog_dist, outline, mean = compare_images(image_path_2, image_path_3)

    # Save and return the data
    return sim, hog_dist, outline, mean


def img_average(img):
    return np.mean(img)


if __name__ == '__main__':
    process_image('D:\\temp\\arvores_de_risco.png', 'D:\\temp\\test.png', 4)

    img = io.imread('C:\\TempX\\TotalDensity.png')
    img = skimage.img_as_float(img)
    # img = img[:, :, 0]
    print(img.shape)
    H, W = img.shape

    im = Image.open('D:\\temp\\test.png').convert("RGB")
    new_img = im.resize((W, H), Image.BICUBIC)
    new_img.save('D:\\temp\\test_2.png')

    img_1 = io.imread('D:\\temp\\test_2.png')
    img_1 = skimage.img_as_float(img_1)
    img_1 = img_1[:, :, 0]

    print(f'mean: {mean_distance(img, img_1)}')
    print(f'ssim: {ssim(img, img_1)}')
    print(f'image avg: {img_average(img)}')

    # Passing the input image in the
    # bicubic function
    # dst = bicubic(img, ratio, a)
    # io.imsave('D:\\temp\\test_2.png', dst)
