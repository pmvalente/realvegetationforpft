import unreal_importer
from Classes.MaterialGen import EParamValue, MaterialCreator, MaterialEditingLib
import unreal
import math
from enum import Enum

landscape_coords = 'landscape_coords'
landscape_scaled_coords = 'scaled_coords'
landscape_scaled_noise_coords = 'scaled_noise_coords'
distance_lerp = 'distance_lerp_value'
macro_variation_value = 'macro_variation'


class EHeightMap(Enum):
    binary_maps = 0
    height_maps = 1


# Create a new layer
# blend input that
# uses height values
def __create_layer_blend_in__(name='default_name', layer_blend_type=unreal.LandscapeLayerBlendType.LB_HEIGHT_BLEND):
    layer_blend = unreal.LayerBlendInput()
    layer_blend.set_editor_property('blend_type', layer_blend_type)
    layer_blend.set_editor_property('layer_name', name)
    return layer_blend


# Assigns a new value
# to a expression
def __assign_editor_value__(material, expression_name, property_name, property_value):
    expression = material.get_expression(expression_name)
    expression.set_editor_property(property_name, property_value)
    return expression


# Assign the editor texture
def __assign_editor_texture__(material, expression_name, texture_path):
    texture = unreal_importer.import_asset(texture_path)
    expression = __assign_editor_value__(material, expression_name, 'texture', texture)
    return expression, texture


def __create_linear_color__(value):
    color = unreal.LinearColor()

    color.set_editor_property('r', value[0])
    color.set_editor_property('g', value[1])
    color.set_editor_property('b', value[2])

    return color


# Set the texture values
# for a specific
def set_layer_values(material, layer_name, texture_name, height_map_name):
    editor_asset_lib = unreal.EditorAssetLibrary()
    layer_color_expression_name = f'texture_{layer_name}'
    layer_height_expression_name = f'height_map_{layer_name}'

    ground_texture = editor_asset_lib.load_asset(texture_name)
    height_texture = editor_asset_lib.load_asset(height_map_name)

    __assign_editor_value__(material, layer_color_expression_name, 'texture', ground_texture)
    __assign_editor_value__(material, layer_height_expression_name, 'texture', height_texture)
    return material


# This function creates
# a layer blend structure
# that assumes a single
# density map combination
def create_landscape_layers_height(material, pos, blend_layers):
    # Create the landscape
    # coords expression
    landscape_coords = 'landscape_coords'
    layers = []

    node_connections = []
    node_names = []
    material.add_node(unreal.MaterialExpressionLandscapeLayerCoords, landscape_coords, pos, layout_mat=False)

    # for every layer we
    # are representing
    # I create two texture
    # nodes, one for the
    # ground texture and
    # another for the height texture
    i = 0

    for layer in blend_layers:
        layer_blend_input = __create_layer_blend_in__(layer)
        layers.append(layer_blend_input)

        texture = f'texture_{layer}'
        height_map = f'height_map_{layer}'

        node_connections.append(f'Layer {layer}')
        node_connections.append(f'Height {layer}')

        node_names.append(texture)
        node_names.append(height_map)

        material.add_node(
            unreal.MaterialExpressionTextureSample,
            texture, (pos[0] + 200, pos[1] + i * 200),
            layout_mat=False)
        material.add_node(
            unreal.MaterialExpressionTextureSample,
            height_map, (pos[0] + 400, pos[1] + i * 200),
            layout_mat=False)
        i += 1

    blend = 'Blend_1'
    material.add_node(
        unreal.MaterialExpressionLandscapeLayerBlend,
        blend, pos=(pos[0] + 600, pos[1]), layout_mat=False)

    blend_expression = material.get_expression(blend)
    blend_expression.set_editor_property('layers', layers)

    for index in range(0, len(node_names)):
        node_name = node_names[index]
        node_input_val = node_connections[index]

        material.connect_expressions(landscape_coords, node_name, '', 'UVs')
        material.connect_expressions(node_name, blend, 'RGB', node_input_val)

    material.layout_material()
    return material, blend


# This function creates
# a layer blend structure
# that assumes a single
# binary map combination
def create_landscape_layers_binary(material, pos, blend_layers):
    # Create the landscape
    # coords expression
    landscape_coords = 'landscape_coords'
    main_height_map = 'main_height_map'
    layers = []

    node_connections = []
    node_names = []
    multiplication_nodes = []

    material.add_node(
        unreal.MaterialExpressionLandscapeLayerCoords,
        landscape_coords, pos, layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionTextureSample,
        main_height_map, (pos[0] + 200, pos[1]),
        layout_mat=False)

    material.connect_expressions(landscape_coords, main_height_map, '', 'UVs')
    i = 0

    for layer in blend_layers:
        layer_blend_input = __create_layer_blend_in__(layer)
        layers.append(layer_blend_input)

        texture = f'texture_{layer}'
        height_map = f'height_map_{layer}'
        multi_node = f'multi_{layer}'

        node_connections.append(f'Layer {layer}')
        node_connections.append(f'Height {layer}')

        node_names.append(texture)
        node_names.append(height_map)
        multiplication_nodes.append(multi_node)

        material.add_node(
            unreal.MaterialExpressionTextureSample,
            texture, (pos[0] + 400, pos[1] + i * 200),
            layout_mat=False)

        material.add_node(
            unreal.MaterialExpressionTextureSample,
            height_map, (pos[0] + 600, pos[1] + i * 200),
            layout_mat=False)

        material.add_node(
            unreal.MaterialExpressionMultiply,
            multi_node, (pos[0] + 800, pos[1] + i * 200),
            layout_mat=False)
        i += 1

    blend = 'Blend_1'
    material.add_node(
        unreal.MaterialExpressionLandscapeLayerBlend,
        blend, pos=(pos[0] + 1000, pos[1]), layout_mat=False)

    blend_expression = material.get_expression(blend)
    blend_expression.set_editor_property('layers', layers)

    for index in range(0, len(node_names)):
        node_name = node_names[index]
        node_input_val = node_connections[index]
        material.connect_expressions(landscape_coords, node_name, '', 'UVs')

        if (index + 1) % 2 == 0:
            multi_index = int(math.floor((index + 1) / 2)) - 1
            multi_node = multiplication_nodes[multi_index]
            material.connect_expressions(node_name, multi_node, 'RGB', 'A')
            material.connect_expressions(main_height_map, multi_node, 'RGB', 'B')
            material.connect_expressions(multi_node, blend, '', node_input_val)

        else:
            material.connect_expressions(node_name, blend, 'RGB', node_input_val)

    material.layout_material()
    return material, blend


def create_landscape_layers(material, pos, blend_layers, layer_type=EHeightMap.height_maps):
    switcher = {
        EHeightMap.height_maps: create_landscape_layers_height,
        EHeightMap.binary_maps: create_landscape_layers_binary
    }
    return switcher[layer_type](material, pos, blend_layers)


def create_distance_from_actor_to_camera(material, pos):
    world_position = 'world_position'
    actor_position = 'actor_position'
    distance = 'distance'

    material.add_node(
        unreal.MaterialExpressionWorldPosition,
        world_position, pos, layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionCameraPositionWS,
        actor_position, (pos[0], pos[1] + 200),
        layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionDistance,
        distance, (pos[0] + 400, pos[1]),
        layout_mat=False)

    material.connect_expressions(world_position, distance, '', 'A')
    material.connect_expressions(actor_position, distance, '', 'B')
    material.layout_material()

    return material, distance


def __get_grass_node__():
    return 0


# This method it's
# responsible for
# setting up the
# grass height
def create_grass_setup(material, pos, blend_layers):
    grass_texture_node = 'grass_texture_node'
    material.add_node(
        unreal.MaterialExpressionTextureSample,
        grass_texture_node, pos, layout_mat=False)

    blend_sample_nodes = []
    i = 0
    for blend_layer in blend_layers:
        blend_sample = f'blend_sample{blend_layer}'
        blend_sample_nodes.append(blend_sample)

        material.add_node(
            unreal.MaterialExpressionLandscapeLayerWeight,
            blend_sample, (pos[0] + 200, pos[1] + i * 200),
            layout_mat=False)
        __assign_editor_value__(material, blend_sample, 'parameter_name', blend_layer)
        i += 1

    grass_output_node = 'grass_output_node'
    material.add_node(
        unreal.MaterialExpressionLandscapeGrassOutput,
        grass_output_node, (pos[0] + 400, pos[1]),
        layout_mat=False)

    grass_types = []
    for blend_layer in blend_layers:
        grass_type = unreal.GrassInput()
        grass_type.set_editor_property('name', blend_layer)
        grass_types.append(grass_type)

    grass_type = unreal.GrassInput()
    grass_type.set_editor_property('name', 'bush')
    grass_types.append(grass_type)
    __assign_editor_value__(material, grass_output_node, 'grass_types', grass_types)

    for i in range(0, len(blend_sample_nodes)):
        blend_sample = blend_sample_nodes[i]
        blend_layer = blend_layers[i]
        material.connect_expressions(blend_sample, grass_output_node, '', blend_layer)

    material.connect_expressions(grass_texture_node, grass_output_node, 'RGB', 'bush')
    material.layout_material()
    return material, '', grass_texture_node


# grass_data = {
#   'class_name' : 'grass_reference'
# }

def test_grass():
    material_pos_start = (-1400, 0)
    # layers_simple = ['fracas', 'resistentes']
    layers = ['l1', 'l2']
    grass_data = {
        'l1': 'DefaultGrassType',
        'l2': 'DefaultGrassType',
        'l3': 'DefaultGrassType',
        'l4': 'DefaultGrassType1',
        'l5': 'DefaultGrassType2'
    }

    # Create the starting material
    material = MaterialCreator()
    grass_setup(material, material_pos_start, grass_data=grass_data)


def grass_setup(material, pos, grass_data=None, path='/RealVegetationForPFT/GrassTypes'):

    if grass_data is None:
        grass_data = {
            'class_name': 'grass_reference'
        }

    layers = grass_data.keys()
    grass_types = {}
    blend_sample_nodes = []
    i = 0

    for layer in layers:
        # Create layer sample node
        blend_sample = f'blend_sample{layer}'
        blend_sample_nodes.append(blend_sample)

        grass_type = grass_data[layer]
        temp = []
        if grass_type in grass_types:
            temp = grass_types[grass_type]

        temp.append(layer)
        grass_types[grass_type] = temp

        material.add_node(
            unreal.MaterialExpressionLandscapeLayerSample,
            blend_sample, (pos[0] + 200, pos[1] + i * 200),
            layout_mat=False)
        __assign_editor_value__(material, blend_sample, 'parameter_name', layer)
        i += 1

    grass_output_node = 'grass_output_node'
    material.add_node(
        unreal.MaterialExpressionLandscapeGrassOutput,
        grass_output_node, (pos[0] + 800, pos[1]),
        layout_mat=False)

    i = 0
    grass_inputs = []
    grass_nodes = []
    grass_type_keys = list(grass_types.keys())

    for grass_layer in grass_type_keys:
        layer_blends = grass_types[grass_layer]
        grass_type = unreal.GrassInput()
        grass_asset = unreal_importer.import_asset(f'{path}/{grass_layer}')

        grass_type.set_editor_property('name', grass_layer)
        grass_type.set_editor_property('grass_type', grass_asset)
        grass_inputs.append(grass_type)

        if len(layer_blends) == 1:
            grass_nodes.append(f'blend_sample{layer_blends[0]}')
            i += 1
            continue

        _, node_connections, blend_node = create_layer_blend(
            material, (pos[0] - 100, pos[1] + i * 200), layer_blends,
            extension=f'grass_{i}', create_landscape_sample=False)

        j = 0
        grass_nodes.append(blend_node)
        for node_input in node_connections:
            material.connect_expressions(
                f'blend_sample{layer_blends[j]}', blend_node,
                '', f'Layer {node_input}', layout_mat=False)
            j += 1

        i += 1

    __assign_editor_value__(material, grass_output_node, 'grass_types', grass_inputs)
    for i in range(0, len(grass_nodes)):
        blend_sample = grass_nodes[i]
        blend_layer = grass_type_keys[i]
        material.connect_expressions(blend_sample, grass_output_node, '', blend_layer, layout_mat=False)

    return material, '', grass_output_node


def __component_mask__(material, name, masked_element='r'):
    exp = material.get_expression(name)
    exp.set_editor_property('r', masked_element == 'r')
    exp.set_editor_property('g', masked_element == 'g')
    exp.set_editor_property('b', masked_element == 'b')
    exp.set_editor_property('a', masked_element == 'a')


# TODO: Amanhã perguntar
#  sobre isto ??? Pq é que
#  se faz sample e scale
#  disto assim? Há alternativas
def create_ortophoto_sample(material, pos, scale):
    landscape_coords_1 = 'landscape_coords_scaled_1'
    landscape_coords_2 = 'landscape_coords_scaled_2'

    material.add_node(
        unreal.MaterialExpressionLandscapeLayerCoords,
        landscape_coords_1, pos, layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionLandscapeLayerCoords,
        landscape_coords_2, (pos[0], pos[1] + 200),
        layout_mat=False)

    exp = material.get_expression(landscape_coords_1)
    exp.set_editor_property('mapping_scale', scale[0])

    exp = material.get_expression(landscape_coords_2)
    exp.set_editor_property('mapping_scale', scale[1])

    mask_1 = 'mask_1'
    mask_2 = 'mask_2'

    material.add_node(
        unreal.MaterialExpressionComponentMask,
        mask_1, (pos[0] + 200, pos[1]),
        layout_mat=False)
    __component_mask__(material, mask_1, 'r')

    material.add_node(
        unreal.MaterialExpressionComponentMask,
        mask_2, (pos[0] + 200, pos[1] + 200),
        layout_mat=False)
    __component_mask__(material, mask_2, 'g')

    append_node = 'orthophoto_append'
    material.add_node(
        unreal.MaterialExpressionAppendVector,
        append_node, (pos[0] + 400, pos[1]),
        layout_mat=False)

    material.connect_expressions(landscape_coords_1, mask_1, '', '')
    material.connect_expressions(landscape_coords_2, mask_2, '', '')
    material.connect_expressions(mask_1, append_node, '', 'A')
    material.connect_expressions(mask_2, append_node, '', 'B')
    material.layout_material()

    return material, append_node


# Alternative way to
# create the scaled
# landscape sampler
def create_ortophoto_sample_alt(material, pos, scale):
    landscape_coords = 'scaled_landscape_coords'
    material.add_node(
        unreal.MaterialExpressionLandscapeLayerCoords,
        landscape_coords, pos, layout_mat=False)

    const_scale = 'orthophto_v2_scale'
    material.add_node(
        unreal.MaterialExpressionConstant2Vector,
        const_scale, (pos[0], pos[1] + 400),
        layout_mat=False)

    exp = material.get_expression(const_scale)
    exp.set_editor_property('r', scale[0])

    exp = material.get_expression(const_scale)
    exp.set_editor_property('g', scale[1])

    multi_node = 'orthophto_multi'
    material.add_node(
        unreal.MaterialExpressionMultiply,
        multi_node, (pos[0] + 400, pos[1]),
        layout_mat=False)

    material.connect_expressions(landscape_coords, multi_node, '', 'A')
    material.connect_expressions(const_scale, multi_node, '', 'B')

    return material, multi_node


# Adds a new divide node
# that let's you divide
# two elements that have
# already been created
def create_divide_node(material, pos, element_a, element_b, index=0, out_a='', out_b=''):
    divide_node_i = f'divide_{index}'
    material.add_node(
        unreal.MaterialExpressionDivide,
        divide_node_i, pos,
        layout_mat=False)

    material.connect_expressions(element_a, divide_node_i, out_a, 'A')
    material.connect_expressions(element_b, divide_node_i, out_b, 'B')

    return material, divide_node_i


# Creates an if node
# that performs an if
# statement with already
# created expressions
def create_if_node(
        material, pos,
        element_a, element_b,
        element_a_equals_b,
        element_a_is_greater_then_b,
        element_a_is_smaller_then_b,
        index=0, out_a='', out_b='',
        out_a_eq_b='', out_a_gt_b='',
        out_a_st_b=''):
    if_node_i = f'if_{index}'
    material.add_node(
        unreal.MaterialExpressionIf,
        if_node_i, pos,
        layout_mat=False)

    material.connect_expressions(element_a, if_node_i, out_a, 'A')
    material.connect_expressions(element_b, if_node_i, out_b, 'B')
    material.connect_expressions(element_a_equals_b, if_node_i, out_a_eq_b, 'A == B')
    material.connect_expressions(element_a_is_smaller_then_b, if_node_i, out_a_st_b, 'A < B')
    material.connect_expressions(element_a_is_greater_then_b, if_node_i, out_a_gt_b, 'A > B')

    return material, if_node_i


# Creates a constant float expression
def create_const_float(material, pos, value, index=0):
    float_const_node = f'float_const_node_{index}'
    material.add_node(
        unreal.MaterialExpressionConstant,
        float_const_node, pos,
        layout_mat=False)

    __assign_editor_value__(material, float_const_node, 'r', value)

    return material, float_const_node


def create_lerp_node(
        material, pos,
        element_a, element_b,
        element_alpha, index=0,
        out_a='', out_b='', out_alpha=''):
    lerp_node_i = f'lerp_{index}'
    material.add_node(
        unreal.MaterialExpressionLinearInterpolate,
        lerp_node_i, pos,
        layout_mat=False)

    material.connect_expressions(element_a, lerp_node_i, out_a, 'A')
    material.connect_expressions(element_b, lerp_node_i, out_b, 'B')
    material.connect_expressions(element_alpha, lerp_node_i, out_alpha, 'alpha')

    return material, lerp_node_i


def debug_create_layers(material, layer_names, pos):
    append_vector_1 = 'append_vector_1'
    append_vector_2 = 'append_vector_2'
    nodes = [[append_vector_1, 'A'],
             [append_vector_1, 'B'],
             [append_vector_2, 'B']]

    material.add_node(
        unreal.MaterialExpressionAppendVector,
        append_vector_1, (pos[0] + 200, pos[1]),
        layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionAppendVector,
        append_vector_2, (pos[0] + 400, pos[1]),
        layout_mat=False)

    material.connect_expressions(append_vector_1, append_vector_2, '', 'A')
    max_element = len(layer_names)
    max_element = max_element if max_element < 4 else 4
    layer_samples = []
    i = j = 0

    for index in range(0, max_element):
        layer_name = f'layer_sample_{layer_names[index]}'
        layer_samples.append(layer_name)

        material.add_node(
            unreal.MaterialExpressionLandscapeLayerSample,
            layer_name, (pos[0], pos[1] + i * 200),
            layout_mat=False)
        __assign_editor_value__(material, layer_name, 'parameter_name', layer_names[index])

        i += 1

    for node in nodes:
        if j <= i:
            material.connect_expressions(layer_samples[j], node[0], '', node[1])
        j += 1

    return material, append_vector_2


def debug_show_layers(material_name='default', layer_names=['fracas', 'resistentes']):
    # Create the starting material
    material = MaterialCreator()
    material_pos_start = (-1400, 0)
    layer_type = EHeightMap.height_maps

    layers = layer_names
    layers.append('outros')
    material, landscape_layers = create_landscape_layers(material, material_pos_start, layers, layer_type=layer_type)

    material_pos_start = (-500, 0)
    material, color = debug_create_layers(material, layer_names, material_pos_start)
    material.connect_property(color, '')

    return material


def create_landscape_material_old(layers_simple=None, layer_type=EHeightMap.height_maps):
    if layers_simple is None:
        layers_simple = ['fracas', 'resistentes']

    material_pos_start = (-1400, 0)
    # layers_simple = ['fracas', 'resistentes']
    layers = layers_simple  # ['fracas', 'resistentes', 'outros']
    layers.append('outros')

    # Create the starting material
    material = MaterialCreator()
    material, landscape_layers = create_landscape_layers(material, material_pos_start, layers, layer_type=layer_type)

    # Creates the distance from actor setup
    material_pos_start = (-1400, 800)
    material, distance_nodes = create_distance_from_actor_to_camera(material, material_pos_start)

    # Creates the scaled othophoto sampler
    material_pos_start = (-2000, 0)
    material, scaled_uv = create_ortophoto_sample(material, material_pos_start, scale=(7199.0, 4500.0))

    # Same thing but optimized (??? I think ???)
    # material_pos_start = (-2000, 0)
    # material, _ = create_ortophoto_sample_alt(material, material_pos_start, scale=(7199.0, 4500.0))

    # Creates the grass setup
    material_pos_start = (-2000, 600)
    material, _, grass_input = create_grass_setup(material, material_pos_start, layers_simple)
    material.connect_expressions(scaled_uv, grass_input, '', 'UVs')

    # creates the distance factor constant
    # TODO: Have to change this
    material_pos_start = (-600, 0)
    material, const_1 = create_const_float(material, material_pos_start, 350000.0)

    # creates another constant
    material_pos_start = (-400, 0)
    material, const_2 = create_const_float(material, material_pos_start, 300000.0, index=1)

    # creates a divide node
    material_pos_start = (-400, 200)
    material, divide = create_divide_node(material, material_pos_start, distance_nodes, const_1)

    # Clamp Values
    clamp_value = 'clamp_value'
    material_pos_start = (-200, 200)
    material.add_node(
        unreal.MaterialExpressionClamp,
        clamp_value, material_pos_start,
        layout_mat=False)
    material.connect_expressions(divide, clamp_value, '', '')

    orthophoto = 'orthophoto_texture_sample'
    material_pos_start = (-1400, 600)
    material.add_node(
        unreal.MaterialExpressionTextureSample,
        orthophoto, material_pos_start,
        layout_mat=False)
    material.connect_expressions(scaled_uv, orthophoto, '', 'UVs')

    material_pos_start = (-200, 600)
    material, lerp_node = create_lerp_node(
        material, material_pos_start, landscape_layers, orthophoto, clamp_value, out_b='RGB')

    material_pos_start = (100, 200)
    material, color = create_if_node(
        material, material_pos_start, const_1, const_2, lerp_node, lerp_node, orthophoto, out_a_st_b='RGB')

    material.connect_property(color, '')

    return material


def create_material_instance(material_instance_path, material_path, name):
    # Crete a new material editing library
    material_editor = MaterialEditingLib()
    material = unreal_importer.import_asset(material_path)

    # Create the material factory to create a new material instance
    factory = unreal.MaterialInstanceConstantFactoryNew()

    # Create the new material instance
    asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
    material_instance = asset_tools.create_asset(name, material_instance_path, None, factory)

    # Set the material instance
    material_editor.set_material_instance_parent(material_instance, material)
    return f'{material_instance_path}/{name}'


def set_parameter_value(
        value, parameter_name,
        material_path,
        create_instance=False,
        materials_folder='/Game/Materials',
        name='MaterialInstance'):

    if create_instance:
        print('Created a new material instance')
        material_path = create_material_instance(materials_folder, material_path, name)

    # Crete a new material editing library
    material_editing_lib = MaterialEditingLib()

    # import the necessary asset
    material = unreal_importer.import_asset(material_path)

    # Set the material instance value
    material_editing_lib.set_material_instance_texture_parameter_value(material, parameter_name, value)

    return material_path, material_editing_lib, material


# This method creates
# the render target material
# that it's responsible for
# creating
def create_render_texture_material():
    # Create the starting material
    material = MaterialCreator()

    # Create the texture
    # coordinate node
    material_pos_start = (-600, 0)
    texture_coord_node = 'texture_coord_node'
    material.add_node(
        unreal.MaterialExpressionTextureSample,
        texture_coord_node, material_pos_start,
        layout_mat=False)

    material_pos_start = (-300, 0)
    texture_sample_node = 'texture_sample_node'
    material.add_node(
        unreal.MaterialExpressionTextureSampleParameter2D,
        texture_sample_node, material_pos_start,
        layout_mat=False)

    material.connect_expressions(texture_coord_node, texture_sample_node, '', 'UVs')
    material.connect_property(texture_sample_node, 'RGB', material_property=unreal.MaterialProperty.MP_EMISSIVE_COLOR)


def create_layer_blend(material, pos, blend_layers, extension='', create_landscape_sample=True):
    layers = []
    node_connections = {}

    if create_landscape_sample:
        material.add_node(
            unreal.MaterialExpressionLandscapeLayerCoords,
            landscape_coords, pos, layout_mat=False)

    i = 0
    for layer in blend_layers:
        layer_blend_input = __create_layer_blend_in__(
            layer, layer_blend_type=unreal.LandscapeLayerBlendType.LB_WEIGHT_BLEND)

        layers.append(layer_blend_input)
        node_connections[layer] = f'Layer {layer}'

        i += 1

    blend = f'Blend_{extension}'
    material.add_node(
        unreal.MaterialExpressionLandscapeLayerBlend,
        blend, pos=(pos[0] + 600, pos[1]), layout_mat=False)

    blend_expression = material.get_expression(blend)
    blend_expression.set_editor_property('layers', layers)

    return material, node_connections, blend


def create_layers_low_end(material, pos, blend_layers):
    # Create the landscape coords expression
    # landscape_coords = 'landscape_coords'
    layers = []

    node_connections = []
    node_names = []
    material.add_node(unreal.MaterialExpressionLandscapeLayerCoords, landscape_coords, pos, layout_mat=False)
    i = 0

    for layer in blend_layers:
        layer_blend_input = __create_layer_blend_in__(
            layer, layer_blend_type=unreal.LandscapeLayerBlendType.LB_WEIGHT_BLEND)
        layers.append(layer_blend_input)

        texture = f'texture_{layer}'
        node_connections.append(f'Layer {layer}')
        node_names.append(texture)

        material.add_node(
            unreal.MaterialExpressionTextureSample,
            texture, (pos[0] + 200, pos[1] + i * 200),
            layout_mat=False)
        i += 1

    blend = 'Blend_1'
    material.add_node(
        unreal.MaterialExpressionLandscapeLayerBlend,
        blend, pos=(pos[0] + 600, pos[1]), layout_mat=False)

    blend_expression = material.get_expression(blend)
    blend_expression.set_editor_property('layers', layers)

    for index in range(0, len(node_names)):
        node_name = node_names[index]
        node_input_val = node_connections[index]

        material.connect_expressions(landscape_coords, node_name, '', 'UVs')
        material.connect_expressions(node_name, blend, 'RGB', node_input_val)

    material.layout_material()
    return material, blend


def create_layer(material, material_data, layer_name, pos, path='/RealVegetationForPFT/Textures/'):
    material_attributes = f'{layer_name}_material_attributes'

    # creates the material attributes
    material.add_node(
        unreal.MaterialExpressionMakeMaterialAttributes,
        material_attributes,
        (pos[0] + 1200, pos[1])
    )

    albedo = f'{layer_name}_albedo'
    i = 0

    # Albedo texture sample;
    material.add_node(
        unreal.MaterialExpressionTextureSample,
        albedo,
        pos
    )
    material.connect_expressions(landscape_coords, albedo, '', 'UVs')
    __assign_editor_texture__(material, albedo, f'{path}{layer_name}')

    if not material_data['is_breaking_tiling']:
        material.connect_expressions(albedo, material_attributes, 'RGB', 'BaseColor')
        # __assign_editor_texture__(material, albedo, f'/RealVegetationForPFT/Textures/{layer_name}')

    # Set the texture parameter

    # Normal texture sample
    if material_data['is_using_normals']:
        expression_name = f'{layer_name}_normals'
        i += 1
        material.add_node(
            unreal.MaterialExpressionTextureSample,
            expression_name,
            (pos[0], pos[1] + 300 * i)
        )
        material.connect_expressions(landscape_coords, expression_name, '', 'UVs')
        material.connect_expressions(expression_name, material_attributes, 'RGB', 'Normal')
        # Set the texture parameter
        __assign_editor_texture__(material, expression_name, f'/RealVegetationForPFT/Textures/{layer_name}_Normal')

    # Ambient Occlusion texture sample
    if material_data['is_using_ambient_occlusion']:
        expression_name = f'{layer_name}_ambient_occlusion'
        i += 1
        material.add_node(
            unreal.MaterialExpressionTextureSample,
            expression_name,
            (pos[0], pos[1] + 300 * i)
        )
        material.connect_expressions(landscape_coords, expression_name, '', 'UVs')
        material.connect_expressions(expression_name, material_attributes, 'RGB', 'AmbientOcclusion')
        # Set the texture parameter
        __assign_editor_texture__(material, expression_name, f'/RealVegetationForPFT/Textures/{layer_name}_AO')

    # Roughness texture sample
    if material_data['is_using_roughness']:
        expression_name = f'{layer_name}_roughness'
        i += 1
        material.add_node(
            unreal.MaterialExpressionTextureSample,
            expression_name,
            (pos[0], pos[1] + 300 * i)
        )
        material.connect_expressions(landscape_coords, expression_name, '', 'UVs')
        material.connect_expressions(expression_name, material_attributes, 'RGB', 'Roughness')
        # Set the texture parameter
        __assign_editor_texture__(material, expression_name, f'/RealVegetationForPFT/Textures/{layer_name}_Roughness')

    # Sets up the tile breaking process
    if material_data['is_breaking_tiling']:
        scaled_albedo = f'{layer_name}_scaled_albedo'
        # Albedo texture sample;
        material.add_node(
            unreal.MaterialExpressionTextureSample,
            scaled_albedo,
            (pos[0] + 300, pos[1])
        )
        material.connect_expressions(landscape_scaled_coords, scaled_albedo, '', 'UVs')
        # Set the texture parameter
        __assign_editor_texture__(material, scaled_albedo, f'/RealVegetationForPFT/Textures/{layer_name}')

        lerp = f'lerp_{layer_name}'
        material.add_node(
            unreal.MaterialExpressionLinearInterpolate,
            lerp,
            (pos[0] + 500, pos[1])
        )
        material.connect_expressions(albedo, lerp, 'RGB', 'A')
        material.connect_expressions(scaled_albedo, lerp, 'RGB', 'B')
        material.connect_expressions(distance_lerp, lerp, '', 'alpha')

        # Connect the final material attribute
        material.connect_expressions(lerp, material_attributes, '', 'BaseColor')

    return material, material_attributes


def distance_lerp_setup(material, pos):
    world_position = 'world_position'
    actor_position = 'actor_position'
    distance = 'distance'

    divide = 'distance_divide'
    power = 'distance_power'
    # clamp = 'distance_clamp'

    distance_parameter = 'distance_parameter'

    material.add_node(
        unreal.MaterialExpressionWorldPosition,
        world_position, pos, layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionCameraPositionWS,
        actor_position, (pos[0], pos[1] + 200),
        layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionDistance,
        distance, (pos[0] + 400, pos[1]),
        layout_mat=False)

    material.connect_expressions(world_position, distance, '', 'A')
    material.connect_expressions(actor_position, distance, '', 'B')

    material.add_node(
        unreal.MaterialExpressionDivide,
        divide, (pos[0] + 600, pos[1]),
        layout_mat=False)

    material.add_node(
        unreal.MaterialExpressionScalarParameter,
        distance_parameter, (pos[0] + 600, pos[1] + 200),
        layout_mat=False)

    __assign_editor_value__(material, distance_parameter, 'parameter_name', 'MaxDistance')
    __assign_editor_value__(material, distance_parameter, 'default_value', 10000.0)

    material.connect_expressions(distance, divide, '', 'A')
    material.connect_expressions(distance_parameter, divide, '', 'B')

    material.add_node(
        unreal.MaterialExpressionPower,
        power, (pos[0] + 800, pos[1]),
        layout_mat=False)

    material.connect_expressions(divide, power, '', 'Base')

    material.add_node(
        unreal.MaterialExpressionClamp,
        distance_lerp, (pos[0] + 1000, pos[1]),
        layout_mat=False)

    material.connect_expressions(power, distance_lerp, '', '')

    return material, distance_lerp


def scaled_coords(
        material, pos, extension=0,
        final_node=landscape_scaled_coords,
        scaled_scale_x='ScaleX', scaled_scale_y='ScaleY'
):

    scale_x = f'scale_x_{extension}'
    material.add_node(
        unreal.MaterialExpressionScalarParameter,
        scale_x,
        pos
    )
    __assign_editor_value__(material, scale_x, 'parameter_name', scaled_scale_x)
    __assign_editor_value__(material, scale_x, 'default_value', 25)

    scale_y = f'scale_y_{extension}'
    material.add_node(
        unreal.MaterialExpressionScalarParameter,
        scale_y,
        (pos[0], pos[1] + 150)
    )
    __assign_editor_value__(material, scale_y, 'parameter_name', scaled_scale_y)
    __assign_editor_value__(material, scale_y, 'default_value', 25)

    append = f'append_1_{extension}'
    material.add_node(
        unreal.MaterialExpressionAppendVector,
        append,
        (pos[0] + 150, pos[1])
    )

    material.connect_expressions(scale_x, append, '', 'A')
    material.connect_expressions(scale_y, append, '', 'B')

    # divide = 'scaled'
    material.add_node(
        unreal.MaterialExpressionDivide,
        final_node,
        (pos[0] + 300, pos[1])
    )

    material.connect_expressions(landscape_coords, final_node, '', 'A')
    material.connect_expressions(append, final_node, '', 'B')

    return material, final_node


def macro_variation(
        material,
        pos, macro_variation_texture=
        '/RealVegetationForPFT/Textures/NodeVariation/T_MacroVariation.T_MacroVariation'
):
    values = [0.2134, 0.05341, 0.002]
    i = 0
    out_expressions = []

    for value in values:
        # TODO : Set editor values

        texture_coordinate = f'texture_coordinate_{i}'
        material.add_node(
            unreal.MaterialExpressionTextureCoordinate,
            texture_coordinate,
            (pos[0], pos[1] + i * 300)
        )

        multiply = f'marco_var_multi_{i}'
        material.add_node(
            unreal.MaterialExpressionMultiply,
            multiply,
            (pos[0] + 200, pos[1] + i * 300)
        )
        __assign_editor_value__(material, multiply, 'const_b', value)
        material.connect_expressions(texture_coordinate, multiply, '', 'A')

        texture = f'marco_var_texture_{i}'
        material.add_node(
            unreal.MaterialExpressionTextureSample,
            texture,
            (pos[0] + 400, pos[1] + i * 300)
        )
        __assign_editor_texture__(material, texture, macro_variation_texture)
        material.connect_expressions(multiply, texture, '', 'UVs')

        add = f'marco_var_add_{i}'
        material.add_node(
            unreal.MaterialExpressionAdd,
            add,
            (pos[0] + 600, pos[1] + i * 300)
        )
        __assign_editor_value__(material, add, 'const_b', 0.5)
        material.connect_expressions(texture, add, 'R', 'A')
        out_expressions.append(add)

        i += 1

    multiply_one = f'marco_var_multi_out_1'
    material.add_node(
        unreal.MaterialExpressionMultiply,
        multiply_one,
        (pos[0] + 800, pos[1])
    )
    material.connect_expressions(out_expressions[1], multiply_one, '', 'A')
    material.connect_expressions(out_expressions[2], multiply_one, '', 'B')

    multiply_two = f'marco_var_multi_out_2'
    material.add_node(
        unreal.MaterialExpressionMultiply,
        multiply_two,
        (pos[0] + 1000, pos[1])
    )
    material.connect_expressions(out_expressions[0], multiply_two, '', 'A')
    material.connect_expressions(multiply_one, multiply_two, '', 'B')

    constant_v3 = 'marco_var_constant_v3'
    material.add_node(
        unreal.MaterialExpressionConstant3Vector,
        constant_v3,
        (pos[0] + 1200, pos[1])
    )

    material.add_node(
        unreal.MaterialExpressionLinearInterpolate,
        macro_variation_value,
        (pos[0] + 1400, pos[1])
    )

    color_value = __create_linear_color__((0.1, 0.1, 0.1))
    __assign_editor_value__(material, constant_v3, 'constant', color_value)

    material.connect_expressions(constant_v3, macro_variation_value, '', 'A')
    material.connect_expressions(multiply_two, macro_variation_value, '', 'alpha')

    return material, macro_variation_value


def create_perlin_mixing(
        material, pos,
        noise_texture_path=
        '/RealVegetationForPFT/Textures/NodeVariation/T_Perlin_Noise_M.T_Perlin_Noise_M'
):

    material, last_node = scaled_coords(
        material, pos, extension=1,
        final_node=landscape_scaled_noise_coords,
        scaled_scale_x='PerlinScaleX', scaled_scale_y='PerlinScaleY'
    )

    texture = 'perlin_texture'
    material.add_node(
        unreal.MaterialExpressionTextureSample,
        texture,
        (pos[0] + 450, pos[1])
    )
    __assign_editor_texture__(material, texture, noise_texture_path)
    material.connect_expressions(last_node, texture, '', 'UVs')

    multiply = 'perlin_multiply'
    material.add_node(
        unreal.MaterialExpressionMultiply,
        multiply,
        (pos[0] + 750, pos[1])
    )

    mat_blend_att = 'mat_blend_att'
    material.add_node(
        unreal.MaterialExpressionScalarParameter,
        mat_blend_att,
        (pos[0] + 750, pos[1] + 200)
    )

    __assign_editor_value__(material, mat_blend_att, 'parameter_name', 'MatBlendAmount')
    __assign_editor_value__(material, mat_blend_att, 'default_value', 1)

    material.connect_expressions(texture, multiply, 'RGB', 'A')
    material.connect_expressions(mat_blend_att, multiply, '', 'B')

    power = 'perlin_power'
    material.add_node(
        unreal.MaterialExpressionPower,
        power,
        (pos[0] + 900, pos[1])
    )

    mat_power_att = 'mat_power_att'
    material.add_node(
        unreal.MaterialExpressionScalarParameter,
        mat_power_att,
        (pos[0] + 900, pos[1] + 200)
    )

    __assign_editor_value__(material, mat_power_att, 'parameter_name', 'MatBlendPower')
    __assign_editor_value__(material, mat_power_att, 'default_value', 1)

    material.connect_expressions(multiply, power, '', 'Base')
    material.connect_expressions(mat_power_att, power, '', 'Exp')

    clamp = 'perlin_clamp'
    material.add_node(
        unreal.MaterialExpressionClamp,
        clamp,
        (pos[0] + 1100, pos[1])
    )
    material.connect_expressions(power, clamp, '', '')

    return material, clamp


def create_all_layers(material, material_data, vegetation_layers, pos):
    # landscape_coords = 'landscape_coords'
    layer_nodes = []

    material.add_node(
        unreal.MaterialExpressionLandscapeLayerCoords,
        landscape_coords,
        pos, layout_mat=False
    )

    if material_data['is_breaking_tiling']:
        material, scaled_coord = scaled_coords(material, (pos[0] + 300, pos[1] - 300))

    material, _ = distance_lerp_setup(material, (pos[0], pos[1] - 600))

    material, _ = macro_variation(material, (pos[0], pos[1] - 1800))

    # layer_data = material_data['layer_data']
    visible_layers = material_data['visible_layers']
    layers = material_data['layers']
    # layer_colors = material_data['layer_colors']

    # Create a layer for every layer we want to represent
    for i in range(len(visible_layers)):
        visible_layer = visible_layers[i]
        material, layer = create_layer(material, material_data, visible_layer, (pos[0] + 400, pos[1] + 1600 * i))
        layer_nodes.append(layer)

    # Create the layer blend node and setup all the necessary values
    material, layer_inputs, blend = create_layer_blend(material, (pos[0] + 1500, pos[1]), vegetation_layers)

    # Blend the dark and lighter tone gras areas
    k = len(visible_layers)
    perlin = ''
    is_first = True
    for i in range(len(vegetation_layers)):
        blends = layers[vegetation_layers[i]]
        is_layer_blend = len(blends) > 1
        layer_input = layer_inputs[vegetation_layers[i]]
        # print(f'AAAAAAAAAAAAAAAAAAAH {layers}')
        layer_output = layer_nodes[blends[0]]

        if is_layer_blend:
            if is_first:
                is_first = False
                material, perlin = create_perlin_mixing(material, (pos[0] + 400, pos[1] + 1600 * k))

            material_blend_attributes = f'material_blend_attributes{i}'
            material.add_node(
                unreal.MaterialExpressionBlendMaterialAttributes,
                material_blend_attributes,
                (pos[0] + 1850, pos[1] + 300 * i), layout_mat=False
            )

            val_1 = layer_nodes[blends[0]]
            val_2 = layer_nodes[blends[1]]

            material.connect_expressions(val_1, material_blend_attributes, '', 'A')
            material.connect_expressions(val_2, material_blend_attributes, '', 'B')
            material.connect_expressions(perlin, material_blend_attributes, '', 'alpha')
            layer_output = material_blend_attributes

        material.connect_expressions(layer_output, blend, '', layer_input)

    return material, blend


# layers = ['layer_1', 'layer_2', 'layer_3']
# data_format = {
#     'is_breaking_tiling': True,
#     'is_changing_specular': True,
#     'is_using_normals': True,
#     'is_using_ambient_occlusion': True,
#     'is_using_roughness': True,
#     'visible_layers':
#         [
#             'Grass', 'DriedGrass', 'ForestGround'
#         ],
#     'layers':
#         {
#             'layer_1': [1, 2],
#             'layer_2': [0],
#             'layer_3': [0]
#         },
# }


def create_specular_node(material, pos, prev_node, prev_node_name):
    mask = 'mask_specular'
    material.add_node(
        unreal.MaterialExpressionComponentMask,
        mask,
        (pos[0] + 2800, pos[1] + 200), layout_mat=False
    )
    material.connect_expressions(prev_node, mask, prev_node_name, '', layout_mat=False)

    clamp = 'clamp_specular'
    material.add_node(
        unreal.MaterialExpressionClamp, clamp,
        (pos[0] + 2975, pos[1] + 200), layout_mat=False
    )
    material.connect_expressions(mask, clamp, '', '', layout_mat=False)

    multiply = 'multiply_specular'
    material.add_node(
        unreal.MaterialExpressionMultiply, multiply,
        (pos[0] + 3200, pos[1] + 200), layout_mat=False
    )
    specular_amount = 'specular_amount'
    material.add_node(
        unreal.MaterialExpressionScalarParameter,
        specular_amount,
        (pos[0] + 3200, pos[1] + 400), layout_mat=False
    )

    __assign_editor_value__(material, specular_amount, 'parameter_name', 'SpecularAmount')
    __assign_editor_value__(material, specular_amount, 'default_value', 0.5)
    __assign_editor_value__(material, clamp, 'max_default', 0.5)

    material.connect_expressions(clamp, multiply, '', 'A', layout_mat=False)
    material.connect_expressions(specular_amount, multiply, '', 'B', layout_mat=False)

    material.connect_property(
        multiply, '',
        material_property=unreal.MaterialProperty.MP_SPECULAR)

    return material


def create_landscape_material_high_end(material_data, material_name='default', layers=None):
    if layers is None:
        layers = []

    # create a new material
    material = MaterialCreator(material_name)

    # create layers
    start_pos = (-3400, 0)
    material, blend = create_all_layers(material, material_data, layers, start_pos)

    # break the material attribute
    material_break_attributes = 'break_attributes'
    material.add_node(
        unreal.MaterialExpressionBreakMaterialAttributes,
        material_break_attributes,
        (start_pos[0] + 2400, start_pos[1]), layout_mat=False
    )
    material.connect_expressions(blend, material_break_attributes, '', 'Attr')

    # Set the material base color
    base_color_input = material_break_attributes
    base_color_name = 'BaseColor'

    if material_data['is_breaking_tiling']:
        multiply = 'macro_variation_multiply_end'
        material.add_node(
            unreal.MaterialExpressionMultiply,
            multiply,
            (start_pos[0] + 2650, start_pos[1]), layout_mat=False
        )
        material.connect_expressions(base_color_input, multiply, base_color_name, 'A')
        material.connect_expressions(macro_variation_value, multiply, '', 'B')

        base_color_input = multiply
        base_color_name = ''

    material.connect_property(
        base_color_input, base_color_name,
        material_property=unreal.MaterialProperty.MP_BASE_COLOR)

    # Set the specular value
    if material_data['is_changing_specular']:
        material = create_specular_node(material, start_pos, base_color_input, base_color_name)

    # Set the roughness
    if material_data['is_using_roughness']:
        material.connect_property(
            material_break_attributes, 'Roughness',
            material_property=unreal.MaterialProperty.MP_ROUGHNESS)

    # Set the normals
    if material_data['is_using_normals']:
        material.connect_property(
            material_break_attributes, 'Normal',
            material_property=unreal.MaterialProperty.MP_NORMAL)

    # Set the ambient occlusion
    if material_data['is_using_ambient_occlusion']:
        material.connect_property(
            material_break_attributes, 'AmbientOcclusion',
            material_property=unreal.MaterialProperty.MP_AMBIENT_OCCLUSION)

    # material.layout_material()
    grass_data = material_data['grass_data']
    grass_setup(material, (start_pos[0] - 1000, start_pos[1]), grass_data=grass_data)
    return material


def create_landscape_material_low_end(material_name='default', layers=None):
    if layers is None:
        layers = []

    # create new material
    material = MaterialCreator(material_name)

    # Create the texture
    # coordinate node
    material_pos_start = (-600, 0)
    material, blend = create_layers_low_end(material, material_pos_start, layers)

    # create blend node
    material.connect_property(blend, '')

    return material


if __name__ == '__main__':
    create_landscape_material_old()
