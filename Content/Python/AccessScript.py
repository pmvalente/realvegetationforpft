from Classes.data_collector import DataCollector, image_format, layer, pixel_type, interpolation, table
from Classes.image_processor import ImageProcessor
from Classes.settings_importer import SettingImporter

from unreal_importer import *
from Classes.shapefile_processor import *
from LandscapeEditor import *
from FoliageEditor import *
from Classes.Level import Level

import os
import numpy as np
import MaterialEditor as me
from Analysis.TimeAnalysisLib import *


# TODO: Implement the auto-import into unreal 
#  also try to play with the auto-setup feature
#  to create the new landscape material

# table_name = 'test_table_1.txt'
# image_path = 'test_image_1.png'
# image_save_path = 'test_image_2.png'
remote_crs = 'EPSG:3857'


#
# bound_x_min = -946914.4277606456
# bound_x_max = 4821995.293980075
# bound_y_min = -811315.1395826909
# bound_y_max = 4886202.397739647
#
# img_size_x = 1024
# img_size_y = 1024


# TODO: Add the option to
#  switch between dpi and
#  resolutions


def add_system_paths():
    # This part of the codes reads
    # the configuration file
    ini_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pcg_config.ini')
    settings = SettingImporter(ini_path)

    # Set the system paths
    paths = settings.set_system_path()

    return paths


# remote_bounds -> Remote bounds
# image_size -> image size
# dc -> Data collector
# ip -> Image processor
# land_cover_classes -> land cover classes
# land_cover_class_names -> land cover class names
def process_clc(
        image_path, table_name,
        remote_bounds,
        image_size, dc, ip,
        land_cover_classes,
        land_cover_class_names):
    add_system_paths()

    # Download the masks through CLC
    dc.get_image(
        layer.CLC,
        image_path,
        remote_bounds, image_size,
        image_format.JPG_PNG,
        interpolation.NEAREST_NEIGHBOR,
        pixel_type.PIXEL_U8)

    # Download the targeted Table
    dc.get_table(
        table_name,
        table.CLC,
        is_json=True)

    ip.read_color_ramp_json(table_name, 'Data')
    color_map = ip.read_clc_legend(land_cover_classes, land_cover_class_names, 'Data')
    paths = ip.apply_color_ramp_to_land_cover(image_path, 'CLC_Mask', color_map, land_cover_class_names)

    return paths


def get_settings():
    return 0


def process_height_map():
    return 0


def get_image_resolution(bounds, dpi):
    return 0


# TODO: Add option for
#  the user decide how
#  he wants to store
#  the image data
def process_data(
        in_image_size='',
        in_bounds='',
        crs='', default_folder='C:\\Temp',
        process_external_data=False,
        apply_mask=False,
        data=None, is_using_custom_classes=False,
        is_a_low_end_system=False, material_data=None,
        foliage_data=None):

    clean_time_table()

    global c
    if data is None:
        data = {}

    if material_data is None:
        material_data = {}

    if foliage_data is None:
        foliage_data = {}

    add_system_paths()

    # This part of the codes reads
    # the configuration file
    ini_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pcg_config.ini')
    settings = SettingImporter(ini_path)

    table_name = settings.file_names['table_name']
    image_path = settings.file_names['image_path']
    image_save_path = settings.file_names['image_save_path']

    bound_x_min = settings.download['bound_x_min']
    bound_y_min = settings.download['bound_y_min']
    bound_x_max = settings.download['bound_x_max']
    bound_y_max = settings.download['bound_y_max']

    img_size_x = settings.download['img_size_x']
    img_size_y = settings.download['img_size_y']
    target_folder = settings.file_names['temp_folder']

    material_name = settings.material['material_name']
    default_ground_texture = '/Game/Textures/default_texture'

    start_crs = 'EPSG:3857'
    applies_mask_to_density_img = apply_mask

    bounds = (float(bound_x_min), float(bound_y_min), float(bound_x_max), float(bound_y_max))
    image_size = (float(img_size_x), float(img_size_y))

    if process_external_data:
        input_sz = in_image_size.split(',')
        if len(input_sz) != 2:
            print('Wrong Format, please input the following format: width,length')
            return

        image_size = (float(input_sz[0]), float(input_sz[1]))

        input_bounds = in_bounds.split(',')
        if len(input_bounds) != 4:
            print('Wrong Format, please input the following format: x_min,y_min,x_max,y_max')
            return

        start_crs = crs
        target_folder = default_folder
        bounds = (float(input_bounds[0]), float(input_bounds[1]), float(input_bounds[2]), float(input_bounds[3]))

    # Download the targeted image
    dc = DataCollector(target_folder)
    remote_bounds = get_bounds(bounds, start_crs, remote_crs)

    # Downloads the data for
    # the tree cover density
    #
    save_time()
    dc.get_image(
        layer.TREE_DENSITY,
        image_path,
        remote_bounds, image_size,
        image_format.JPG_PNG,
        interpolation.CUBIC_COVOLUTION,
        pixel_type.PIXEL_U8)

    # Download the targeted Table
    dc.get_table(
        table_name,
        table.TREE_DENSITY,
        is_json=True)

    save_time()
    value, _ = get_time()
    clean_time_table()
    print(f'DURATION DOWNLOAD : {value}')

    save_time()
    ip = ImageProcessor(target_folder)
    color_ramp = ip.read_color_ramp_json(table_name, 'Data')
    path_to_file, image_name = ip.apply_color_ramp_to_image(image_path, 'image_1.png', color_ramp)
    save_time()
    value, _ = get_time()
    clean_time_table()
    print(f'DURATION COLOR PROCESSING : {value}')

    # save_time()
    import_texture(path_to_file, '/Game/Textures')
    land_cover_classes, land_cover_class_names = settings.get_land_cover_table(False)
    class_data = data if is_using_custom_classes else land_cover_classes
    path_tuples = get_render(bounds, start_crs, class_data=class_data, resolution=image_size)
    # save_time()
    # value, _ = get_time()
    # clean_time_table()
    # print(f'DURATION RENDERS : {value}')

    binary_maps_paths = []
    density_maps_paths = []

    # TODO: Find the classes that appear 2 or more
    #  times and get their renders then add a new
    #  step where you multiply their values
    classes = {''}
    layer_names = class_data.keys()

    repeated_classes = {}
    render_targets = {}
    nb_of_duplicates = 0

    save_time()
    for class_name in layer_names:
        compile_classes = class_data[class_name]
        for c in compile_classes:
            if c in classes:
                temp = 1
                if c in repeated_classes:
                    temp = repeated_classes[c]

                repeated_classes[c] = temp + 1
                nb_of_duplicates += 1
                render_targets[c] = [c]

            classes.add(c)

    # print(render_targets)
    duplicate_path_tuples = []
    if nb_of_duplicates > 0:
        # print('kdnmfgspojdfngspidofpfodjsgndjkfngpskjdfnpgkjsdfn    AAAAAAAAAAAAAAAAAA')
        duplicate_path_tuples = get_render(bounds, start_crs, class_data=render_targets, resolution=image_size)

    image_list = []
    # print(f'fjdgopusdfnhgiodfhbgodufhbgosdhfbgojhsdfbgojhdfbgojshdfbgojshdfbgjohsdfbg {data} {duplicate_path_tuples}')
    for path_tuple in duplicate_path_tuples:
        path = path_tuple[0]
        class_name = path_tuple[1]

        class_res = 1 / repeated_classes[class_name]
        image = ip.get_duplicate_masks(path_to_file, path, class_res)
        image_list.append(image)

    # This part basically ensures that if we don't have
    # trees in a specific class, we don't import the tree
    # density
    for path_tuple in path_tuples:

        path = path_tuple[0]
        class_name = path_tuple[1]

        if applies_mask_to_density_img:
            tree_path = foliage_data[class_name]['asset_path']
            if tree_path:
                density_maps_paths.append(path)
            else:
                binary_maps_paths.append(path)
        else:
            binary_maps_paths.append(path)

    for path in density_maps_paths:
        ip.apply_binary_mask(path_to_file, path, image_list)

    for path in binary_maps_paths:
        ip.invert_color(path, True)

    for path_tuple in path_tuples:
        path = path_tuple[0]
        import_texture(path, '/Game/Textures')

    class_names = class_data.keys()
    for c in class_names:
        asset = import_asset(f'/Game/Textures/{c}.{c}')
        asset.set_editor_property('filter', unreal.TextureFilter.TF_NEAREST)
    save_time()
    value, _ = get_time()
    clean_time_table()
    print(f'DURATION MULTIPLICATION : {value}')

    classes = list(class_data.keys())
    mask_type = me.EHeightMap.height_maps if applies_mask_to_density_img else me.EHeightMap.binary_maps
    material_objet = None

    if not verify_asset(f'/Game/Materials/{material_name}'):
        classes.append('outros')
        material = me.create_landscape_material_high_end(material_data, material_name, classes)

        material_objet = material.get_material()
    else:
        material_objet = import_asset(f'/Game/Materials/{material_name}')

    # material = me.create_landscape_material(layers_simple=classes, layer_type=mask_type)
    # for c in classes:
    #     height_map_path = f'/Game/Textures/{c}'
    #     me.set_layer_values(material, c, default_ground_texture, height_map_path)

    level = Level()
    level.set_landscape_material(material_objet)
    return classes


def set_landscape_material(material_path='/Game/Materials/default'):
    material = import_asset(material_path)
    level = Level()
    level.set_landscape_material(material)


def set_landscape_proxies_material(material_path='/Game/Materials/default'):
    material = import_asset(material_path)
    level = Level()
    level.set_proxies_materials(material)


def set_landscape(
        layers=None,
        render_target_path='',
        render_target_material_name='',
        render_target_material_inst_name='',
        render_target_material_folder='',
        folder='/Game/Foliage',
        size_x=256, size_y=256):

    if layers is None:
        layers = []

    level = Level()
    render_target_material_path = f'{render_target_material_folder}/{render_target_material_name}'
    static_mesh = import_asset('/Game/KiteDemo/Environments/Trees/Tree_Stump_01/Tree_Stump_01.Tree_Stump_01')
    foliage_helper = create_layers_1(layers, static_mesh, target_folder=folder)

    # TODO : Switch this later
    for layer in layers:
        layer_path = f'/Game/Textures/{layer}'

        # TODO : First fill every layer
        # Fills a specific layer
        # set_weights(layer, level, layer_path, material_path=white_material_path)

        # Imports the correct layer weights
        set_weights(
            layer, level,
            layer_path,
            render_target_path,
            render_target_material_inst_name,
            render_target_material_folder,
            render_target_material_path)

    # spawner = foliage_helper.get_spawner()
    # level.set_spawner_values(spawner)
    # foliage_helper.simulate(100)


def get_number_of_tiles(tile_names=None):
    if tile_names is None:
        tile_names = ['tile_x0_y0']

    return get_max_value(tile_names)


def store_tile_res(tile_names=None):
    if tile_names is None:
        tile_names = ['tile_x0_y0']

    width, height = get_number_of_tiles(tile_names)

    # This part of the codes reads
    # the configuration file
    ini_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pcg_config.ini')
    settings = SettingImporter(ini_path)
    settings.store_tile_res(width, height)


def load_tile_res():
    # This part of the codes reads
    # the configuration file
    ini_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pcg_config.ini')
    settings = SettingImporter(ini_path)
    width, height = settings.get_tile_res()
    return width, height


def set_landscape_weights(
        layers=None,
        render_target_path='',
        render_target_material_name='',
        render_target_material_inst_name='',
        render_target_material_folder='',
        texture_path='/Game/Textures',
        tile_name='',
        size_x=256, size_y=256):

    level = Level()
    render_target_material_path = f'{render_target_material_folder}/{render_target_material_name}'

    # get the stored tile width and height
    width, height = load_tile_res()

    for l in layers:
        layer_path = f'{texture_path}/{l}'
        # print(layer_path)

        # Imports the correct layer weights
        set_weights(
            l, level,
            layer_path,
            render_target_path,
            render_target_material_inst_name,
            render_target_material_folder,
            render_target_material_path,
            tile_name, width, height,
            size_x, size_y)


# data = {
#     'layer_name_1': {
#         'asset_path': '/Game/KiteDemo/Environments/Trees/Tree_Stump_01/Tree_Stump_01.Tree_Stump_01',
#         'vegetation_value_1': 1
#     }
# }


def create_foliage_assets(
        layers=None,
        folder='/Game/Foliage',
        foliage_data=None):

    if foliage_data is None:
        foliage_data = {
            f'{layers[0]}': {
                'asset_path': '/Game/KiteDemo/Environments/Trees/Tree_Stump_01/Tree_Stump_01.Tree_Stump_01'
            }
        }

    level = Level()
    # static_mesh = import_asset('/Game/KiteDemo/Environments/Trees/Tree_Stump_01/Tree_Stump_01.Tree_Stump_01')
    foliage_helper = create_layers_1(layers, target_folder=folder, foliage_data=foliage_data)

    spawner = foliage_helper.get_spawner()
    level.set_spawner_values(spawner)
    foliage_helper.simulate(100)


def create_export_images(
        image_path='C:\\Users\\olive\\Desktop\\HeightMaps\\HeightMap.png',
        color_ramp_path='C:\\Users\\olive\\Desktop\\HeightMaps\\ColorRamp.png',
        new_image_path='C:\\Users\\olive\\Desktop\\HeightMaps\\ColorRamp_2.png', ):
    target_folder = ''
    image_processor = ImageProcessor(target_folder)

    color_ramp = image_processor.load_color_ramp(color_ramp_path).reshape(100, 3)
    image_processor.apply_color_ramp_to_image(image_path, new_image_path, color_ramp, save_image=False)


# This method is used for
# downloading the data used in the
# COS system
def cos_download(bounds, dc, image_path, image_size, settings, target_folder):
    color_map, _ = dc.get_cos_data(bounds, image_path, image_size)
    ip = ImageProcessor(target_folder)

    land_cover_classes, land_cover_class_names = settings.get_land_cover_table(has_extension=False)
    final_color_table = {}

    for class_name in land_cover_class_names:
        classes = land_cover_classes[class_name]
        temp = []
        # print(f'{classes}')

        for c in classes:
            if c in color_map.keys():
                temp.append(color_map[c])

        final_color_table[class_name] = np.array(temp)
    paths = ip.apply_color_ramp_to_land_cover(image_path, 'CLC_Mask', final_color_table, land_cover_class_names)

    return paths


if __name__ == '__main__':
    # process_data()
    create_export_images()
