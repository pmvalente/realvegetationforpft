array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

# Caso generico para qualquer vetor / tabela
def reshape_array(array, nb_of_lines):
    # Guardar o tamanho do vetor / tabela
    size = len(array)

    # Calcular o tamanho de uma linha
    line_size = int(size / nb_of_lines)

    # Calcula o resto da divisão anterior
    mod = int(size % nb_of_lines)
    final_array = []

    # Este ciclo vai gerar todas as 
    # linhas da tabela
    for i in range(0, nb_of_lines):
        
        # Verificar se e o ultimo 
        # elemento do array
        is_final_element = i == (nb_of_lines - 1)
     
        # Seleciaona a parte do array 
        # que precisas de usar

        # neste caso e preciso fazer um passo
        # adicional para caso seja o ultimo elemento
        # do vetor inicial, para garantir que todos 
        # os valores sao inceridos
        line = array[i * line_size : ((i + 1) * line_size) + mod * is_final_element]
        final_array.append(line)

    return final_array
    
# Caso simples para se o 
# vetor / tabela tiver um 
# numero de elementos par
def reshape_array_simple(array, nb_of_lines):
    # Guardar o tamanho do vetor / tabela
    size = len(array)

    # Calcular o tamanho de uma linha
    line_size = int(size / nb_of_lines)
    final_array = []

    for i in range(0, nb_of_lines):
        # Seleciaona a parte do array 
        # que precisas de usar
        line = array[i * line_size : ((i + 1) * line_size)]
        final_array.append(line)

    return final_array

# Caso simples para se o 
# vetor / tabela tiver um 
# numero de elementos par
def reshape_array_correct(array, nb_of_lines):

    # Guardar o tamanho do vetor / tabela
    size = len(array)

    # Calcula o resto da divisão anterior
    mod = int(size % nb_of_lines)
    array = array + mod * [0]
    size = len(array)

    # Calcular o tamanho de uma linha
    line_size = int(size / nb_of_lines)
    final_array = []

    for i in range(0, nb_of_lines):
        # Seleciaona a parte do array 
        # que precisas de usar
        line = array[i * line_size : ((i + 1) * line_size)]
        final_array.append(line)

    return final_array

print(reshape_array(array, 7))
print(reshape_array(array, 2))
print(reshape_array_simple(array, 2))
print(reshape_array_correct(array, 7))
print(reshape_array_correct(array, 2))

