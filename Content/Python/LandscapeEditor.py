import MaterialEditor as me
import unreal
from unreal_importer import *
from Classes.Level import Level
from Classes.RenderTargetHelper import RenderTargetHelper


def set_weights(
        target='Layer_1',
        level=None,
        target_texture_path='/Game/Textures/...',
        render_target_path='/Game/Textures/RenderTargetSetup/image_1',
        material_instance_name='RenderTarget',
        material_folder='/Game/Textures/RenderTargetSetup',
        material_path='/Game/Textures/RenderTargetSetup/RenderTarget',
        tile_name='tile_x0_y0',
        tiles_width=2, tiles_height=2,
        size_x=256, size_y=256):

    # Create the level class
    level = Level() if level is None else level

    # Verify if the necessary material to render the
    # texture target it's already created

    texture = import_asset(target_texture_path)
    material_instance_path = f'{material_folder}/{material_instance_name}'

    is_asset_created = verify_asset(material_instance_path)
    material_path = material_path if not is_asset_created else material_instance_path
    # path = material_path if is_asset_created else material_folder

    material_path, material_editing_lib, mat = me.set_parameter_value(
        texture, 'Texture',
        material_path, not is_asset_created,
        material_folder,
        material_instance_name)

    x_value, y_value = convert_tile_name(tile_name, tiles_width - 1, tiles_height - 1)
    material_editing_lib.set_material_instance_scalar_parameter_value(mat, 'TileHeight', float(tiles_height))
    material_editing_lib.set_material_instance_scalar_parameter_value(mat, 'TileWidth', float(tiles_width))

    material_editing_lib.set_material_instance_scalar_parameter_value(mat, 'TilePosX', float(x_value))
    material_editing_lib.set_material_instance_scalar_parameter_value(mat, 'TilePosY', float(y_value))

    # Generate the necessary layer info objects
    render_target_helper = RenderTargetHelper(material_path)
    render_target = render_target_helper.bake()

    # Generate the renderer helper class
    # Render the target image, and then ...
    level.set_layer_weights(render_target, target)
    # level.set_proxies_layer_weights(render_target_path, target)


# This method receives a
# string containing the name
# the level being loaded
def get_tile_position(tile_name='tile_x0_y0'):
    elements = tile_name.split('_')

    x_value = int(elements[1][1])
    y_value = int(elements[2][1])

    return [x_value, y_value], x_value, y_value


# get the max value from the list
# of level names
def get_max_value(level_names=None):
    if level_names is None:
        level_names = ['tile_x0_y0']

    x_values = []
    y_values = []
    for level_name in level_names:
        _, x, y = get_tile_position(level_name)

        x_values.append(x)
        y_values.append(y)

    return max(x_values), max(y_values)


def convert_tile_name(tile_name, max_tile_width, max_tile_height):
    _, x, y = get_tile_position(tile_name)

    x_v, y_v = convert_tile_position(x, y, max_tile_width, max_tile_height)

    return x_v, y_v  # convert_tile_position(x, y, max_tile_width, max_tile_height)


def convert_tile_position(tile_pos_x, tile_pos_y, max_tile_width, max_tile_height):
    return tile_pos_x, max_tile_height - tile_pos_y


def fill_layer(target, index, layer_path):
    # this method fills the necessary layer
    # with a value
    return 0


def export_weights(target='resistentes'):
    # Create the level class
    level = Level()

    # Create the landscape material
    level.get_layer_weights('/Game/Textures/RenderTargetSetup/image_2', target)
