import unreal
import Classes.Level as lvl


def export_texture(asset_path='/Game', export_path=''):

    texture = import_asset(asset_path)
    write_options = unreal.ImageWriteOptions()

    write_options.set_editor_property('async_', False)
    write_options.set_editor_property('compression_quality', 1)
    write_options.set_editor_property('format', unreal.DesiredImageFormat.PNG)
    write_options.set_editor_property('overwrite_file', True)

    texture.export_to_disk(export_path, write_options)


# TODO: Create a class to import the data
def import_texture(texture_path, target_folder):
    asset_tools = unreal.AssetToolsHelpers.get_asset_tools()

    # plits = texture_path.split('/')
    # ast_name = splits[len(splits) - 1]

    print('Importing file: ' + texture_path + '.')
    import_task = unreal.AssetImportTask()

    import_task.set_editor_property('filename', texture_path)
    import_task.set_editor_property('destination_path', target_folder)
    # import_task.set_editor_property('filter', unreal.TextureFilter.TF_NEAREST)

    asset_tools.import_asset_tasks([import_task])
    # print(f'HELP ME PLEASE: {boas}')


# This function verifies if an asset
# it's already created and
def verify_asset(asset_path):
    editor_asset_lib = unreal.EditorAssetLibrary()
    return editor_asset_lib.does_asset_exist(asset_path)


# This method
# gets the render
# Target texture
def get_render_target(texture_path='/Game/Textures/RenderTargetSetup/RenderTexture'):
    editor_asset_lib = unreal.EditorAssetLibrary()
    rt = None
    if editor_asset_lib.does_asset_exist(texture_path):
        rt = editor_asset_lib.load_asset(texture_path)

    return rt


def import_asset(asset_path='/Game'):
    editor_asset_lib = unreal.EditorAssetLibrary()
    asset = None
    if editor_asset_lib.does_asset_exist(asset_path):
        asset = editor_asset_lib.load_asset(asset_path)

    return asset


def save_asset_from_path(folder_path, asset_name):
    asset_path = f'{folder_path}/{asset_name}'
    save_asset(asset_path)


def save_asset(asset_path=''):
    unreal.EditorAssetLibrary.save_asset(asset_path, only_if_is_dirty=False)


def set_render_target_resolution(res, texture_path='/Game/Textures/RenderTargetSetup/RenderTexture'):
    rt = get_render_target(texture_path)

    rt.set_editor_property('address_x', res[0])
    rt.set_editor_property('address_y', res[1])
