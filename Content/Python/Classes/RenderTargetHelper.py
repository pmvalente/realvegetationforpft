import unreal_importer
import unreal
import Classes.Level as lvl


class RenderTargetHelper:
    def __init__(self, material_path):
        # self.render_target = unreal_importer.import_asset(render_target_path)
        self.material = unreal_importer.import_asset(material_path)

    def bake(self, size_x=256, size_y=256):
        # Get an arbitrary actor from the level
        level = lvl.Level()
        actor = level.get_all_actors()[0]

        # Create the render target
        render_target = unreal.RenderingLibrary.create_render_target2d(actor, width=size_x, height=size_y)

        # Bake the render target
        unreal.RenderingLibrary.draw_material_to_render_target(actor, render_target, self.material)

        return render_target

    def set_material_parameters(self):
        return 0
