import numpy as np
import json as json
import cv2 as cv

import os
import errno
import base64
import skimage.viewer


from skimage import io
from skimage.color import rgb2hsv
from Analysis.TimeAnalysisLib import *
from matplotlib import pyplot as plt

# TODO: - Delete all extra unecessary
#  files after reading their contents;

delete_files = True
threshold = 100


def calculate_distance(i1, i2):
    return (i2 - i1) ** 2


def unmap_nearest(img, rgb):
    d = np.sum(np.abs(img[np.newaxis, ...] - rgb[:, np.newaxis, np.newaxis, :]), axis=-1)
    i = np.argmin(d, axis=0)
    return i / (rgb.shape[0] - 1)


def calculate_dominant_color(img, nb_colors=1):
    pixels = np.float32(img.reshape(-1, 3))

    # Setup the kmeans values
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    flags = cv.KMEANS_RANDOM_CENTERS

    # Calculate the kmeans values
    _, labels, palette = cv.kmeans(pixels, nb_colors, None, criteria, 10, flags)
    _, counts = np.unique(labels, return_counts=True)
    return palette[np.argmax(counts)]


class ImageProcessor:
    def __init__(self, target_folder):
        # IMPLEMENT
        self.target_folder = target_folder
        print('Starting image processor')

    def invert_color(self, image_path, as_alpha=False):
        image = io.imread(image_path)
        if as_alpha:
            image = image[:, :, 0:3]

        image = 255 - image
        io.imsave(image_path, image)

    def load_color_ramp(self, color_ramp_path, is_full_path=False):
        path = color_ramp_path if is_full_path else os.path.join(self.target_folder, color_ramp_path)
        img = io.imread(path)
        return img

    def apply_color_ramp_to_image(self, img_path, new_image, color_ramp, f=True, save_image=True):

        # Get Image
        img = io.imread(os.path.join(self.target_folder, img_path))
        img = skimage.img_as_ubyte(img)

        # Create a white background
        img_height, img_width = img.shape[:2]
        background = np.zeros((img_height, img_width, 4), dtype="uint8") + 255

        # Store and normalize the images alpha 
        #  and apply a background to the image
        img_a = img[..., 3] / 255.0
        img = background * (1 - img_a)[..., np.newaxis] + img * img_a[..., np.newaxis]
        img = img[:, :, :3]

        if save_image:
            io.imsave(os.path.join(self.target_folder, 'color_ramp.png'), np.array([color_ramp]))

        # save_time()
        new_img = unmap_nearest(img, color_ramp)
        # save_time()
        # value, _ = get_time()
        # clean_time_table()
        # print(f'DURATION AAAAAAAAAAAAAAAAAAAAAAA : {value}')
        path = os.path.join(self.target_folder, new_image)
        io.imsave(path, new_img)
        print(f'image saved to {path}')

        return path, new_image

    def apply_color_ramp_to_land_cover(self, img_path, new_image, color_table, class_names):
        img = io.imread(os.path.join(self.target_folder, img_path))
        img = skimage.img_as_ubyte(img)
        images = self.apply_color(img, color_table, class_names)

        paths = []

        for class_name in class_names:
            if class_name not in images:
                continue

            path = os.path.join(self.target_folder, f'{class_name}.png')
            paths.append(path)

            io.imsave(path, images[class_name])
            print(f'image saved to {path}')

        return paths

    def apply_color(self, image, color_table, class_names):

        img_height, img_width = image.shape[:2]
        hsv = rgb2hsv(image[:, :, 0:3])
        hue_channel = hsv[:, :, 0]

        saturation_channel = hsv[:, :, 1]
        final = {}

        for class_name in class_names:

            if class_name not in color_table.keys():
                continue

            colors = color_table[class_name]
            mask = np.zeros((img_height, img_width, 3), dtype="uint8")

            for color in colors:
                hsv_color = rgb2hsv(color[0:3])
                lower = np.clip(hsv_color - (1 / 255), 0, 255)
                upper = np.clip(hsv_color + (1 / 255), 0, 255)

                hue_lower_val = lower[0]
                hue_upper_val = upper[0]

                saturation_lower_val = lower[1]
                saturation_upper_val = upper[1]

                filtered_image = (hue_channel < hue_upper_val) * 1
                filtered_image = filtered_image * (hue_lower_val < hue_channel) * 1

                filtered_image = filtered_image * (saturation_channel < saturation_upper_val) * 1
                filtered_image = filtered_image * (saturation_lower_val < saturation_channel) * 1

                mask[filtered_image == 0] = (255, 255, 255)

                # plt.imshow(mask)
                # plt.show()

            final[class_name] = mask  # final_img

        return final

    def read_color_ramp(self, file_name, ramp_size):
        color_table = []
        i = 0
        # TODO: change the path to account
        #  for the .ini path
        with open(os.path.join(self.target_folder, file_name)) as inputfile:
            for line in inputfile:
                split_line = line.split(',')
                i += 1
                if i < ramp_size:
                    color_table.append(float(split_line[1]))
                    color_table.append(float(split_line[2]))
                    color_table.append(float(split_line[3]))

        return np.array(color_table).reshape(ramp_size - 1, 3)

    def process_legend(self, img_paths):
        color_table = []

        for img_path in img_paths:
            # Get the dominant color
            img = io.imread(img_path)[:, :, :-1]
            dominant = calculate_dominant_color(img)

            color_table.append(dominant[0])
            color_table.append(dominant[1])
            color_table.append(dominant[2])

        return color_table

    def get_duplicate_masks(self, original_image_path, mask_path, fraction):
        original_image = io.imread(mask_path)

        # Convert image into a fraction of what it is
        original_image = skimage.img_as_float(original_image)[:, :, 0:3]
        temp_image = original_image
        original_image = ((1 - original_image) * fraction + temp_image)[:, :, 0]
        io.imsave(mask_path, original_image)

        return original_image

    # Original image & maks -> path to the images
    def apply_binary_mask(
            self, original_image_path,
            mask_path, duplicate_layer_list):
        original_image = io.imread(original_image_path)
        original_image = skimage.img_as_ubyte(original_image)

        mask = io.imread(mask_path)
        mask = skimage.img_as_ubyte(mask)[:, :, 0:3]
        mask = (255 - mask)[:, :, 0]

        dim = (mask.shape[1], mask.shape[0])
        original_image = cv.resize(original_image, dim, interpolation=cv.INTER_AREA)

        final_image = np.where(mask > 0, original_image, 0)
        print(final_image.shape)

        for image in duplicate_layer_list:
            # image = cv.resize(image, dim, interpolation=cv.INTER_LANCZOS4)
            final_image = skimage.img_as_float(final_image)
            final_image = np.multiply(final_image, image)

        io.imsave(mask_path, final_image)

        return final_image

    def plot_hoc(self, image_path):
        image = io.imread(image_path)
        # plt.hist(image.ravel(), bins=256)
        # plt.show()

    def read_color_ramp_json(self, file_name, data_folder):
        print(f'opening file {file_name}')
        img_paths = []

        # TODO: change the path to account
        #  for the .ini path
        with open(os.path.join(self.target_folder, file_name)) as json_file:
            obj = json.loads(json.load(json_file))

            # Read the legend
            legend = obj['layers'][0]['legend']

            for element in legend:

                img_data = base64.b64decode(element['imageData'])
                # print(img_data)
                label = element['label'] + '.png'
                img_path = f'{data_folder}\\{label}'

                # TODO: change the path to
                #  account for the .ini path
                path = os.path.join(self.target_folder, img_path)
                if not os.path.exists(os.path.dirname(path)):
                    try:
                        os.makedirs(os.path.dirname(path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise

                with open(path, 'wb') as f:
                    f.write(img_data)
                    img_paths.append(path)

        color_table = self.process_legend(img_paths)
        ret = np.array(color_table).reshape(len(legend), 3)
        return ret

    # TODO: This is kinda useless ... Remove this
    def read_clc_legend(self, id_table, class_names, data_folder):
        ret = {}

        for class_name in class_names:
            # Read the file path
            images = id_table[class_name]
            color_table = []

            for image in images:
                # get the image path
                img_path = f'{data_folder}\\{image}'
                path = os.path.join(self.target_folder, img_path)

                # Get the dominant color
                img = io.imread(path)[:, :, :-1]
                dominant = calculate_dominant_color(img)

                color_table.append(dominant[0])
                color_table.append(dominant[1])
                color_table.append(dominant[2])

            temp = np.array(color_table).reshape(len(images), 3)
            ret[class_name] = temp

        return ret

    def read_label_json(self, file_name, ramp_size):
        print(f'opening file {file_name}')

        # TODO: change the path to account
        #  for the .ini path
        with open(os.path.join(self.target_folder, file_name)) as json_file:
            obj = json.loads(json.load(json_file))

            # Read the features and get the uid
            fields = obj['fields']

            oid = fields[0]['name']
            value = fields[1]['name']
            # print(oid + ' ' + value)

            # Read the content of the json file
            # and store it in a matrix
            features = obj['features']
            id_table = []

            for feature in features:
                atributes = feature['attributes']
                id_table.append(atributes[value])
                # print(feature)

            return id_table, oid, value
