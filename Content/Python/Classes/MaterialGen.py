import unreal
from enum import Enum


class EParamValue(Enum):
    set_scalar_value = 0
    set_texture_value = 1
    set_vector_value = 2


# Thing that allows
#  you to access the
#  Material editor

@unreal.uclass()
class MaterialEditingLib(unreal.MaterialEditingLibrary):
    pass


# Get a list of the paths to all
#  material instances.
def material_instances():
    instances = list()
    path = "/Game/"

    for asset in unreal.EditorAssetLibrary.list_assets(path):
        asset_data = unreal.EditorAssetLibrary.find_asset_data(asset)
        if asset_data.asset_class == "MaterialInstanceConstant":
            print(asset_data)
            instances.append(asset)

    return instances


# Iterate over Static Bool & Switch
#  Material Expressions for the given
#  material to return a dictionary for
#  parameter names and their default values.
def get_static_switch_values(material):
    parameters = dict()
    filter_on = (
        unreal.MaterialExpressionStaticBoolParameter,
        unreal.MaterialExpressionStaticSwitchParameter
    )

    # Iterate loaded objects that are child of material
    # and matches filter types.
    it = unreal.ObjectIterator()
    for x in it:
        if x.get_outer() == material:
            if isinstance(x, filter_on):
                name = str(x.get_editor_property("parameter_name"))
                value = x.get_editor_property("default_value")
                parameters.update({name: value})

    return parameters


def create_material(name='default'):
    # Create the material editing factory
    factory = unreal.MaterialFactoryNew()

    # Create the new material
    asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
    material = asset_tools.create_asset(name, '/Game/Materials', None, factory)
    return material


def add_node(material, node_type, pos=(0, 0), material_edit=None, is_node_created=False):
    if not is_node_created:
        # Create the material editing library
        material_edit = MaterialEditingLib()

    # print(pos)
    # add the multiply node to the lib
    multiply_expression = material_edit.create_material_expression(
        material, node_type,
        node_pos_x=pos[0],
        node_pos_y=pos[1]
    )
    return multiply_expression


def connect_nodes(node_l, node_r, input_l_name, input_r_name, material_edit=None, is_node_created=False):
    if not is_node_created:
        # Create the material editing library
        material_edit = MaterialEditingLib()

    result = material_edit. \
        connect_material_expressions(
        node_l, input_l_name,
        node_r, input_r_name
    )

    return result


class MaterialCreator:
    def __init__(self, name='default'):
        # create material
        self.material = create_material(name)

        # Initiate meme lib
        self.material_editor = MaterialEditingLib()

        # ... Initialize other stuff
        self.active_nodes = {}
        self.node_count = 0
        self.material_instance = None

    def get_keys(self):
        return self.active_nodes.keys()

    def get_material(self):
        return self.material

    # Layout the material nodes
    #  in a grid pattern
    def layout_material(self):
        self.material_editor.layout_material_expressions(self.material)

    # Returns the resulting expression
    def get_expression(self, node_name):
        return self.active_nodes[node_name]

    # add a new node
    #  to this material
    def add_node(self, node_type, name='', pos=(0, 0), layout_mat=True):
        if not name:
            name = f'node_{self.node_count}'

        if name in self.active_nodes.keys():
            return

        node_ref = add_node(self.material, node_type, pos, self.material_editor, True)
        self.active_nodes[name] = node_ref
        self.node_count += 1

        if layout_mat:
            self.layout_material()

    # Connects two expressions
    #  from this material
    def connect_expressions(self, node_l, node_r, input_l_name, input_r_name, layout_mat=True):
        if node_l not in self.active_nodes or \
                node_r not in self.active_nodes:
            return

        node_ref_0 = self.active_nodes[node_l]
        node_ref_1 = self.active_nodes[node_r]
        connect_nodes(node_ref_0, node_ref_1, input_l_name, input_r_name)

        if layout_mat:
            self.layout_material()

    # Create a material instance that
    # can be hooked up to a renderer
    def create_material_instance(self, name, path='/Game/Materials'):

        # Create the material factory to
        # create a new material instance
        factory = unreal.MaterialInstanceConstantFactoryNew()

        # Create the new material instance
        asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
        material_instance = asset_tools.create_asset(name, path, None, factory)

        # Set the material instance
        self.material_instance = material_instance
        self.material_editor.set_material_instance_parent(self.material_instance, self.material)

        return self.material_instance

    # Sets the value of
    # a specific expression
    def set_expression_value(self, expression_name, property_name, value):
        if property_name not in self.active_nodes.keys():
            return

        expression = self.active_nodes[expression_name]
        expression.set_editor_property(property_name, value)

    # Get the input names
    # for a specific material
    def get_inputs_for_expression(self, expression_name):
        return self.material_editor. \
            get_inputs_for_material_expression(self.material, self.active_nodes[expression_name])

    # Set the parameter value of
    # the active material instance
    def set_material_instance_value(self, value_type, parameter_name, value):

        switcher = {
            EParamValue.set_scalar_value: self.material_editor.set_material_instance_scalar_parameter_value,
            EParamValue.set_texture_value: self.material_editor.set_material_instance_texture_parameter_value,
            EParamValue.set_vector_value: self.material_editor.set_material_instance_vector_parameter_value
        }

        set_value = switcher.get(value_type, lambda: "Invalid Type")
        set_value(self.material_instance, parameter_name, value)

    # This method connects an existing expression
    # To a material property
    def connect_property(
            self, from_expression, from_output_name,
            material_property=unreal.MaterialProperty.MP_BASE_COLOR):

        expression = self.active_nodes[from_expression]
        self.material_editor.connect_material_property(expression, from_output_name, material_property)

        return 0

    # This method returns
    # the texture parameter
    # names of the material
    def get_texture_parameter_names(self):
        return self.material_editor.get_texture_parameter_names(self.material)


if __name__ == '__main__':
    create_material()
