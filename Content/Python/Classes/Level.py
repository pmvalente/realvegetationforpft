import unreal


# https://docs.unrealengine.com/4.26/en-US/PythonAPI/class/EditorLevelLibrary.html
# https://docs.unrealengine.com/4.26/en-US/PythonAPI/class/Actor.html#unreal.Actor
# https://docs.unrealengine.com/4.26/en-US/PythonAPI/class/Landscape.html


@unreal.uclass()
class EditorLevelLib(unreal.EditorLevelLibrary):
    pass


# this method imports all the respective
# layers to landscape in the scene
def set_layers(landscape, layer_names, layer_paths):
    return 0


# this method sets the material
# to the landscape
def set_landscape_material(landscape, material):
    return 0


def test():
    level = Level()
    actor_1 = level.get_selected_actors()
    print(actor_1)
    print(type(actor_1[0]))

    actor_1 = level.get_landscape()
    print(actor_1)
    print(type(actor_1))

    # level.set_landscape_material(material=)


def get_actor_of_type(actors, actor_type):
    target = None
    if len(actors) == 0:
        return None

    for actor in actors:
        is_instance = isinstance(actor, actor_type)
        if is_instance:
            target = actor

    return target


class Level:
    def __init__(self):
        self.level_editor = EditorLevelLib()
        self.landscape = None
        self.landscape_gizmos = None
        self.spawner = None
        self.foliage_component = None
        self.landscape_streaming_proxy = None
        self.landscape_streaming_proxies = []

    def get_selected_actors(self):
        return self.level_editor.get_selected_level_actors()

    def get_actor_form_path(self, path):
        return self.level_editor.get_actor_reference(path)

    def get_all_actors(self):
        return self.level_editor.get_all_level_actors()

    def reset_landscape_streaming_proxy(self):
        self.landscape_streaming_proxy = None

    # returns the first actor
    # of type landscape it
    # finds in the level
    def get_landscape(self):
        actors = self.get_all_actors()
        if len(actors) == 0:
            return None

        landscape = None
        landscape_gizmos = None

        for actor in actors:
            is_instance = isinstance(actor, unreal.Landscape) or isinstance(actor, unreal.LandscapeProxy)
            if is_instance:
                landscape = actor
                self.landscape = landscape

            is_landscape_gizmos = isinstance(actor, unreal.LandscapeGizmoActiveActor)
            if is_landscape_gizmos:
                landscape_gizmos = actor
                self.landscape_gizmos = landscape_gizmos

        return landscape, landscape_gizmos

    def get_landscape_proxies(self):
        actors = self.get_all_actors()
        if len(actors) == 0:
            return None

        landscape_proxies = []

        for actor in actors:
            is_instance = isinstance(actor, unreal.LandscapeStreamingProxy)
            if is_instance:
                landscape = actor
                landscape_proxies.append(actor)

        self.landscape_streaming_proxies = landscape_proxies
        return landscape_proxies

    def set_landscape_proxy_material(self, material):
        if self.landscape_streaming_proxy is None:
            self.landscape_streaming_proxy = get_actor_of_type(self.get_all_actors(), unreal.LandscapeStreamingProxy)

        if self.landscape_streaming_proxy is None:
            return

        self.landscape_streaming_proxy.set_editor_property('landscape_material', material)
        return self.landscape_streaming_proxy

    def set_proxies_materials(self, material):
        landscape_proxies = self.get_landscape_proxies()

        for proxy in landscape_proxies:
            proxy.set_editor_property('landscape_material', material)

    # This method sets up the
    # material for the landscape
    # in the editor
    def set_landscape_material(self, material):

        if self.landscape is None:
            self.get_landscape()

        if self.landscape is None:
            return None

        self.landscape.set_editor_property('landscape_material', material)
        return self.landscape

    def set_layer_info(self, layer_infos):
        if self.landscape_gizmos is None:
            self.get_landscape()

        if self.landscape_gizmos is None:
            return

        # Assign Layer infos
        # self.landscape_gizmos.layer_infos = layer_infos
        return self.landscape_gizmos

    def get_layer_info(self):
        if self.landscape_gizmos is None:
            self.get_landscape()

        if self.landscape_gizmos is None:
            return

        # get the landscape gizmos layer_info
        return self.landscape_gizmos.layer_infos

    def set_layer_weights(self, render_target, layer_name):
        if self.landscape is None:
            self.get_landscape()

        if self.landscape is None:
            return None

        # Loads the render texture
        editor_asset_lib = unreal.EditorAssetLibrary()
        # render_target = editor_asset_lib.load_asset(target_path)
        self.landscape.landscape_import_weightmap_from_render_target(render_target, layer_name)

    def set_proxies_layer_weights(self, target_path, layer_name):
        landscape_proxies = self.get_landscape_proxies()
        editor_asset_lib = unreal.EditorAssetLibrary()
        render_target = editor_asset_lib.load_asset(target_path)

        for proxy in landscape_proxies:
            proxy.landscape_import_weightmap_from_render_target(render_target, layer_name)
        return 0

    def get_layer_weights(self, target_path, layer_name):
        if self.landscape is None:
            self.get_landscape()

        if self.landscape is None:
            return None

        # Loads the render texture
        editor_asset_lib = unreal.EditorAssetLibrary()
        render_target = editor_asset_lib.load_asset(target_path)
        self.landscape.landscape_import_weightmap_from_render_target(render_target, layer_name)

    # Gets the foliage
    # spawner from
    # the level
    def get_spawner(self):
        # Set the class values
        actors = self.get_all_actors()
        if len(actors) == 0:
            return None

        for actor in actors:
            is_instance = isinstance(actor, unreal.ProceduralFoliageVolume)
            if is_instance:
                landscape = actor
                self.spawner = landscape

        if self.spawner is None:
            return None, None

        self.foliage_component = self.spawner.get_editor_property('procedural_component')
        return self.spawner, self.foliage_component

    def set_spawner_values(self, spawner):
        if self.spawner is None:
            self.get_spawner()

        if self.spawner is None:
            return

        self.foliage_component.set_editor_property('foliage_spawner', spawner)
