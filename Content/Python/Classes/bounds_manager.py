# The objective of this class is to receive a
#  bound in a certain projection and 
#  return the 

import fiona
import geopandas as gpd
from shapely.geometry import Polygon

cos_path = 'C:\\Tese-Projeto\\Tese_pessoal\\Tese_Test_Project 4.26 - 3\\Content\\Python\\Data\\COS - 2018 - PT\\COS2018_v1.shp'

# Bounds are defined the
#  following way :
#  (xmin, ymin, xmax, ymax)
def get_bounds(bounds, base_coord_type, coord_type):

    bound_x_min = bounds[0]
    bound_y_min = bounds[1]
    bound_x_max = bounds[2]
    bound_y_max = bounds[3]
    
    clip_polygon = Polygon([
        (bound_x_min, bound_y_min),
        (bound_x_max, bound_y_min),
        (bound_x_max, bound_y_max),
        (bound_x_min, bound_y_max),
        (bound_x_min, bound_y_min)])

    poly_gdf = gpd.GeoDataFrame([1], geometry=[clip_polygon], crs=base_coord_type)
    poly_gdf.crs = {'init' :base_coord_type}
    print(int(coord_type.split(':')[1]))
    poly_gdf = poly_gdf.to_crs(epsg=int(coord_type.split(':')[1]))
    
    temp_bound = poly_gdf.geometry[0].bounds

    return temp_bound

# if __name__ == '__main__':
#     get_bounds((-946914.4277606456, 4821995.293980075, -811315.1395826909, 4886202.397739647), 'EPSG:3763', 'EPSG:3035')