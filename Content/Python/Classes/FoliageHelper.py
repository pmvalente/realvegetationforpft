import unreal
import unreal_importer as ui


# Vegetation class -> A way to represent
#  the current vegetation class ...
#  it must have attributes for every element
#  we want to represent in the world
#  So we need data for ...

# Creates a new
# spawner to a specific folder
def create_spawner(spawner_name='default_spawner', spawner_path='/Game'):
    full_path = f'{spawner_path}/{spawner_name}'
    # if ui.verify_asset(full_path):
    #     spawner = ui.import_asset(full_path)
    #     return spawner

    # Creates the spawner factory
    spawner_factory = unreal.ProceduralFoliageSpawnerFactory()

    # Create the new spawner
    asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
    spawner = asset_tools.create_asset(spawner_name, spawner_path, None, spawner_factory)

    return spawner


# create a new static mesh foliage
def create_static_mesh_foliage(static_mesh_name='default_mesh', static_mesh_path='/Game'):
    full_path = f'{static_mesh_path}/{static_mesh_name}'
    # if ui.verify_asset(full_path):
    #     static_mesh = ui.import_asset(full_path)
    #     return static_mesh

    # Creates a new layer
    foliage_type_factory = unreal.FoliageType_InstancedStaticMeshFactory()

    # Create the new spawner
    asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
    static_mesh = asset_tools.create_asset(static_mesh_name, static_mesh_path, None, foliage_type_factory)

    return static_mesh


class FoliageHelper:
    def __init__(self, spawner_name='default_spawner', spawner_path='/Game'):
        self.spawner = create_spawner(spawner_name, spawner_path)
        self.foliage_layers = {}

    def get_spawner(self):
        return self.spawner

    def get_foliage_meshes(self):
        return self.foliage_layers

    # Create a new spawner
    def create_spawner(self, spawner_name='default_spawner', spawner_path='/Game'):
        self.spawner = create_spawner(spawner_name, spawner_path)
        self.foliage_layers = {}
        return self.spawner

    # Create new layer with
    # a specific name
    def create_layer(self, layer_name='default_mesh', target_path='/Game'):
        foliage_type = create_static_mesh_foliage(layer_name, target_path)

        # Set layer type value...
        layer_names = foliage_type.get_editor_property('landscape_layers')
        layer_names.append(layer_name)
        foliage_type.set_editor_property('landscape_layers', layer_names)

        self.foliage_layers[layer_name] = foliage_type
        return foliage_type

    def create_layers(self, layer_names, target_path):
        for layer in layer_names:
            self.create_layer(layer, target_path)

    def set_layer_mesh(self, layer_name, mesh, vegetation_data):
        foliage_type = self.foliage_layers[layer_name]
        foliage_type.set_editor_property('mesh', mesh)

        foliage_type.set_editor_property('align_to_normal', False)
        foliage_type.set_editor_property('can_grow_in_shade', False)

        foliage_type.set_editor_property('cast_dynamic_shadow', True)
        foliage_type.set_editor_property('cast_shadow', True)

        foliage_type.set_editor_property('overlap_priority', vegetation_data['overlap_priority'])
        foliage_type.set_editor_property('shade_radius', vegetation_data['shade_radius'])

        foliage_type.set_editor_property('collision_radius', vegetation_data['collision_radius'])
        foliage_type.set_editor_property('average_spread_distance', vegetation_data['average_spread_distance'])

        foliage_type.set_editor_property('seeds_per_step', vegetation_data['seeds_per_step'])
        foliage_type.set_editor_property('can_grow_in_shade', vegetation_data['can_grow_in_shade'])

        float_interval = unreal.FloatInterval()
        data = vegetation_data['tree_scale']
        float_interval.set_editor_property('min', data[0])
        float_interval.set_editor_property('max', data[1])
        foliage_type.set_editor_property('procedural_scale', float_interval)

        age_range = vegetation_data['age_range']
        foliage_type.set_editor_property('max_initial_age', age_range[0])
        foliage_type.set_editor_property('max_age', age_range[1])

        float_interval = unreal.FloatInterval()
        data = vegetation_data['z_offset']
        float_interval.set_editor_property('min', data[0])
        float_interval.set_editor_property('max', data[1])
        foliage_type.set_editor_property('z_offset', float_interval)

        # foliage_type.set_editor_property('height', True)
        # foliage_type.set_editor_property('max_age', True)

    def set_layer_values(self, layer_name, vegetation):
        foliage_type = self.foliage_layers[layer_name]

        # Read the vegetation simulation
        # values and create a new vegetation
        # layer
        return 0

    def set_spawner_values(self, foliage_names, use_all=False):
        # go through every foliage
        # type and set the spawner
        # values
        layer_names = self.foliage_layers.keys() if use_all else foliage_names
        foliage_types = []

        for layer_name in layer_names:
            foliage_type = self.foliage_layers[layer_name]
            foliage_type_object = unreal.FoliageTypeObject()
            foliage_type_object.set_editor_property('foliage_type_object', foliage_type)
            foliage_types.append(foliage_type_object)

        self.spawner.set_editor_property('foliage_types', foliage_types)

    def simulate(self, num_steps=1):
        self.spawner.simulate(num_steps)
