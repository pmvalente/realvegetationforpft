import configparser
import sys
import os

# from Classes.settings_importer import *
# import_paths()

def import_paths():

    # This part of the codes reads
    # the configuration file
    ini_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..\\pcg_config.ini')
    settings = SettingImporter(ini_path)

    # Set the system paths
    paths = settings.set_system_path()
    return paths


class SettingImporter:
    def __init__(self, path):
        config = configparser.ConfigParser()
        config.read(path)

        self.parser = config
        self.path = path

        self.download = config['DOWNLOAD']
        self.file_names = config['FILE_NAMES']
        self.land_cover = config['LAND_COVER']
        self.material = config['MATERIAL']
        self.system_paths = config['SYSTEM_PATHS']
        self.res = config['TILE_SETTINGS']

    def get_land_cover_table(self, has_extension=True):
        nb_of_layers = int(self.land_cover['nb_of_layers'])
        names = []
        dic = {}

        for i in range(0, nb_of_layers):

            nb_of_merged_layers = int(self.land_cover[f'nb_of_merged_layers_{i}'])
            layer_name = self.land_cover[f'layer_{i}_name']
            elements_per_layer = []

            for j in range(0, nb_of_merged_layers):
                elements_per_layer.append(self.land_cover[f'layer_id_{i}{j}'] + ('.png' if has_extension else ''))

            dic[layer_name] = elements_per_layer
            names.append(layer_name)

        return dic, names

    def set_system_path(self):

        paths = []
        nb_of_paths = int(self.system_paths['nb_of_paths'])

        for i in range(0, nb_of_paths):
            path = self.system_paths[f'system_path_{i + 1}']
            sys.path.append(path)
            paths.append(path)

        return paths

    def get_tile_res(self):
        config = configparser.ConfigParser()
        config.read(self.path)

        self.parser = config
        self.res = config['TILE_SETTINGS']

        return int(self.res['width']), int(self.res['height'])

    def store_tile_res(self, width, height):

        self.parser.set('TILE_SETTINGS', 'width', f'{width + 1}')
        self.parser.set('TILE_SETTINGS', 'height', f'{height + 1}')

        cfgfile = open(self.path, 'w')
        # use flag in case case you need to avoid white space
        self.parser.write(cfgfile, space_around_delimiters=False)
        cfgfile.close()
