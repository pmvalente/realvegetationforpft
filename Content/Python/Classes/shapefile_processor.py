import pandas as pd
import fiona
import geopandas as gpd

from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from shapely.geometry import Polygon

from Classes.bounds_manager import *
import os

# (-50000,50000,-50000,50000)
dpi = 200

temporary_folder = 'c:\\tempX'
shape_file_path = 'C:\\Tese-Projeto\\Tese_pessoal\\Tese_Test_Project 4.26 - 3\\Plugins\\RealVegetationForPFT\\Content\\Python\\Data\\COS - 2018 - PT\\COS2018_v1.shp'

table_classes = {
    'arvores_resistentes': ['5.1.1.4', '5.1.1.3'],
    'arvores_de_risco': ['5.1.1.5', '5.1.1.6']
}


def set_bounds(
        bounds, data,
        file_name='cos_renderer.png',
        resolution=(1280, 606)):
    bound_x_min = bounds[0]
    bound_y_min = bounds[1]
    bound_x_max = bounds[2]
    bound_y_max = bounds[3]

    clip_polygon = Polygon([
        (bound_x_min, bound_y_min),
        (bound_x_max, bound_y_min),
        (bound_x_max, bound_y_max),
        (bound_x_min, bound_y_max),
        (bound_x_min, bound_y_min)])

    plt.figure()
    cmap = ListedColormap(['black'], name='allblack')
    ax = plt.axes([0, 0, 1, 1], frameon=False)

    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.autoscale(tight=True)

    poly_gdf = gpd.GeoDataFrame([1], geometry=[clip_polygon], crs=data[0].crs)
    poly_gdf.boundary.plot(ax=ax, color="white")

    for element in data:
        data_in_aoi = gpd.overlay(element, poly_gdf, how='intersection')
        data_in_aoi.plot(ax=ax, cmap=cmap)

    img_path = os.path.join(temporary_folder, file_name)
    figure = plt.gcf()
    figure.set_size_inches(resolution[0] / dpi, resolution[1] / dpi)
    plt.savefig(img_path, bbox_inches='tight', pad_inches=0, dpi=dpi)

    return img_path


def get_render(bounds, input_crs, class_data=None, resolution=(1280, 606)):
    if class_data is None:
        class_data = table_classes

    data = gpd.read_file(shape_file_path)
    bounds = get_bounds(bounds, input_crs, data.crs['init'])

    # data = data.to_crs(input_crs)
    # data = data.to_crs("EPSG:3035")
    # data = data.to_crs("EPSG:3857")

    paths = []
    class_names = class_data.keys()

    # print(class_data)

    for class_name in class_names:
        temp = []
        compile_classes = class_data[class_name]
        for c in compile_classes:
            temp.append(data[data.COS2018_n4 == c])

        path = set_bounds(bounds, temp, f'{class_name}.png', resolution=resolution)
        paths.append((path, class_name))

    return paths


if __name__ == '__main__':
    get_render((-946914.4277606456, 4821995.293980075, -811315.1395826909, 4886202.397739647), 'EPSG:3035')
