import json
import os
import urllib
import urllib.request
from enum import Enum

import numpy as np
import urllib3
# from matplotlib import pyplot as plt

from skimage import io, img_as_ubyte
from skimage.filters.rank import median
from skimage.morphology import disk

from skimage.color import rgb2hsv
from skimage.color import hsv2rgb
from skimage.color.adapt_rgb import adapt_rgb, each_channel
from sklearn.cluster import KMeans

delete_files = True


# TODO: Extend this code to make it work
#  with COS in order to do this I have to
#  work with the following API: 
#  http://mapas.dgterritorio.pt/wms-inspire/cos2018v1?service=WMS&REQUEST=GetCapabilities&VERSION=1.3.0

# Notas: ler sobre wms : https://docs.geoserver.org/latest/en/user/services/wms/basics.html
# Exemplo de um possivel url para aceder ao mapa: http://mapas.dgterritorio.pt/wms-inspire/cos2018v1?language=por&version=1.3.0&service=WMS&request=GetMap&layers=COS2018v1&format=image/png&crs=EPSG:3763&bbox=-170000,-320000,200000,280000&width=1000&height=1000
# Exemplo de outro URL possivel que já funcemina: https://snig.dgterritorio.gov.pt/rndg/proxy?url=http://mapas.dgterritorio.pt/wms-inspire/cos2018v1?language=por&&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=COS2018v1.0&STYLES=default&WIDTH=286&HEIGHT=286&CRS=EPSG:3857&BBOX=-851346.0664117566,4916286.339874501,-848613.4426505615,4919018.963635697
# Exemplo de outra alternativa: http://mapas.dgterritorio.pt/wms-inspire/cos2018v1?language=por&&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=COS2018v1.0&STYLES=default&WIDTH=286&HEIGHT=286&CRS=EPSG:3857&BBOX=-851346.0664117566,4916286.339874501,-848613.4426505615,4919018.963635697
# Documentação importante : https://enterprise.arcgis.com/en/server/latest/publish-services/windows/communicating-with-a-wms-service-in-a-web-browser.htm


@adapt_rgb(each_channel)
def median_each(image, disk):
    return median(image, disk)


class layer(Enum):
    # TODO : The url's bellow are for the 2018 tree density maps
    TREE_DENSITY = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_TreeCoverDensity_2018/ImageServer/exportImage'
    # TODO : The url's bellow are for the 2015 tree density maps
    # TREE_DENSITY = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_TreeCoverDensity_2015/MapServer/export'
    TREE_TYPE = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_DominantLeafType_2018/ImageServer/exportImage'
    GRASSLAND = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_Grassland_2018/ImageServer/exportImage'
    CLC = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/Corine/CLC2018_WM/MapServer/export'
    HEIGHT_MAP = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/Elevation/EUElev_DEM_V11/MapServer/export'


class table(Enum):
    # TODO : The url's bellow are for the 2018 tree density maps
    TREE_DENSITY = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_TreeCoverDensity_2018/ImageServer/legend'
    # TODO : The url's bellow are for the 2015 tree density maps
    # TREE_DENSITY = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_TreeCoverDensity_2015/MapServer/legend'
    TREE_TYPE = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_DominantLeafType_2018/ImageServer/rasterAttributeTable'
    GRASSLAND = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic/HRL_Grassland_2018/ImageServer/rasterAttributeTable'
    CLC = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/Corine/CLC2018_WM/MapServer/legend'


class image_format(Enum):
    JPG_PNG = 'jpgpng'
    PNG = 'png'
    PNG_8 = 'png8'
    PNG_24 = 'png24'
    PNG_32 = 'png32'
    JPG = 'jpg'
    BMP = 'bmp'
    GIF = 'gif'
    TIFF = 'tiff'
    BIP = 'bip'
    BSQ = 'bsq'
    LERC = 'lerc'


class pixel_type(Enum):
    PIXEL_C128 = 'C128'
    PIXEL_C64 = 'C64'
    PIXEL_F32 = 'F32'
    PIXEL_F64 = 'F64'
    PIXEL_S16 = 'S16'
    PIXEL_S32 = 'S32'
    PIXEL_S8 = 'S8'
    PIXEL_U1 = 'U1'
    PIXEL_U16 = 'U16'
    PIXEL_U2 = 'U2'
    PIXEL_U32 = 'U32'
    PIXEL_U4 = 'U4'
    PIXEL_U8 = 'U8'


class interpolation(Enum):
    BILINEAR_INTERPOLATION = '+RSP_BilinearInterpolation'
    CUBIC_COVOLUTION = 'RSP_CubicConvolution'
    MAJORITY = 'RSP_Majority'
    NEAREST_NEIGHBOR = 'RSP_NearestNeighbor'


# Generate a DataCollector with
# predifined url's
def generate_with_urls(urls, path):
    temp = DataCollector(path)
    # URL to access th tree density HRL
    temp.url_tree_density = urls['url_tree_density']
    temp.url_tree_density_table = urls['url_tree_density_table']

    # URL to access th tree density HRL
    temp.url_tree_type = urls['url_tree_type']
    temp.url_tree_type_table = urls['url_tree_type_table']

    # URL to access the grassland HRL
    temp.url_grassland = urls['url_grassland']
    temp.url_grassland_table = urls['url_grassland_table']

    # URL to access the CLC
    temp.url_clc = urls['url_clc']

    # return
    return temp


# TODO: I want a method to make a image request from the API,
#  a method to request the table from a specific, so you give the
#  method the name of the layer you want to retrieve, and the
#  specific bounds (and other parameters) you want to specify

def f(x, color, color_set, index):
    # Get the lower and upper bound
    lower = np.clip(color - 15 / 255, 0, 1)
    upper = np.clip(color + 15 / 255, 0, 1)

    is_correct = (lower[0] < x[0]) * 1
    is_correct = is_correct * (x[0] < upper[0]) * 1

    is_correct = is_correct * (lower[1] < x[1]) * 1
    is_correct = is_correct * (x[1] < upper[1]) * 1

    # if the element in the table
    #  was already marked, it stays
    #  the same, if
    color_set[index] = (1 - (1 - color_set[index]) * (1 - is_correct))
    return is_correct


def count_instances(image, color):
    hsv_color = rgb2hsv(color[0:3])
    lower = np.clip(hsv_color - (5 / 255), 0, 255)
    upper = np.clip(hsv_color + (5 / 255), 0, 255)

    filtered_image = (image[:, :, 0] < upper[0]) * 1
    filtered_image = filtered_image * (lower[0] < image[:, :, 0]) * 1

    filtered_image = filtered_image * (image[:, :, 1] < upper[1]) * 1
    filtered_image = filtered_image * (lower[1] < image[:, :, 1]) * 1

    unique, counts = np.unique(filtered_image, return_counts=True)
    d = dict(zip(unique, counts))
    return d[1]


class DataCollector:
    # Default URL to access th tree density HRL
    # TODO : The url's bellow are for the 2018 
    #  tree density maps
    default_tree_density_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic' \
                               '/HRL_TreeCoverDensity_2018/ImageServer/exportImage '
    default_tree_density_table_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic' \
                                     '/HRL_TreeCoverDensity_2018/ImageServer/rasterAttributeTable '

    # Default URL to access th tree density HRL
    default_tree_type_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic' \
                            '/HRL_DominantLeafType_2018/ImageServer/exportImage '
    default_tree_type_table_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic' \
                                  '/HRL_DominantLeafType_2018/ImageServer/rasterAttributeTable '

    # Default URL to access the grassland HRL
    default_grassland_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic' \
                            '/HRL_Grassland_2018/ImageServer/exportImage '
    default_grassland_table_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/GioLandPublic' \
                                  '/HRL_Grassland_2018/ImageServer/rasterAttributeTable '

    # Default URL to access the CLC
    deafault_clc_url = 'https://image.discomap.eea.europa.eu/arcgis/rest/services/Corine/CLC2018_WM/MapServer/export'

    # Class constructor
    def __init__(self, target_folder):
        self.target_folder = target_folder

        # URL to access th tree density HRL
        self.tree_density_url = self.default_tree_density_url
        self.tree_density_table_url = self.default_tree_density_table_url

        # URL to access th tree density HRL
        self.tree_type_url = self.default_tree_type_url
        self.tree_type_table_url = self.default_tree_type_url

        # URL to access the grassland HRL
        self.grassland_url = self.default_grassland_url
        self.grassland_table_url = self.default_grassland_table_url

        # URL to access the CLC
        self.clc_url = self.deafault_clc_url

        # Defines the file format we are
        # trying to access the default image
        self.f = 'image'

    def __store_data(self, request, target, read_block_size=1024):
        # Save the result to a png file
        with open(os.path.join(self.target_folder, target), 'wb') as out:
            while True:
                data = request.read(read_block_size)
                if not data:
                    break
                out.write(data)

        print(f'File Saved to {target}.')

    def __create_request(
            self, bounds,
            image_size,
            image_format,
            interpolation,
            pixel_type):

        return {
            'bbox': f'{bounds[0]},{bounds[1]},{bounds[2]},{bounds[3]}',
            'size': f'{image_size[0]},{image_size[1]}',
            'format': image_format,
            'interpolation': interpolation,
            'pixelType': pixel_type,
            'f': self.f
        }

    # This method is not has fast has the second
    #  implementation, it returns the total nb of
    #  maps present on a given map
    def __get_nb_of_colors(
            self, target_f):
        image = io.imread(os.path.join(self.target_folder, target_f))[:, :, 0:3]
        # image = median_each(image, disk(4))

        n_colors = 83
        arr = image.reshape((-1, 3))
        kmeans = KMeans(n_clusters=n_colors, random_state=42).fit(arr)
        labels = kmeans.labels_
        centers = kmeans.cluster_centers_
        image = centers[labels].reshape(image.shape).astype('uint8')

        image = rgb2hsv(image)
        arr = image.reshape((-1, 3))
        color_set = np.zeros(len(arr))

        table = []
        color_ramp = []

        for i in range(0, len(arr)):
            color = arr[i]
            if len(arr) - np.sum(color_set) < 900:
                break
            if color_set[i]:
                continue
            else:
                temp = np.array([f(arr[j], color, color_set, j) for j in range(0, len(arr))])
                if np.sum(temp) > 300:
                    temp = temp.reshape(image.shape[:2])
                    index = np.where(temp == 1)

                    table.append((index[0][0], index[1][0]))
                    color_ramp.append(hsv2rgb(color) * 255)

                    # plt.imshow(temp)
                    # plt.show()

        print(f'{table}, {color_ramp}')
        return table, color_ramp, image

    def __load_color_table(
            self, bounds,
            target_f,
            image_size,
            coordinate_format='EPSG:3857'):
        image = io.imread(os.path.join(self.target_folder, target_f))

        print(f'nb of colors: {len(np.unique(image.reshape(-1, 4)))}')
        image = median_each(image, disk(10))

        print(f'nb of colors: {len(np.unique(image.reshape(-1, 4)))}')
        n_colors = 83
        arr = image.reshape((-1, 4))
        kmeans = KMeans(n_clusters=n_colors, random_state=42).fit(arr)

        labels = kmeans.labels_
        centers = kmeans.cluster_centers_
        image = centers[labels].reshape(image.shape).astype('uint8')
        print(f'nb of colors: {len(np.unique(image.reshape(-1, 4)))}')

        hsv = rgb2hsv(image[:, :, 0:3])
        img_height, img_width = image.shape[:2]

        # plt.imshow(image)
        # plt.show()

        _table = []
        color_ramp = [image[0, 0]]

        final_values = {}
        final_counts = {}

        ret_values = {}
        class_names = []

        # Load  all the necessary colors
        for i in range(0, img_height):
            for j in range(0, img_width):

                color = image[i, j]
                is_new_color = True

                # TODO: Switch this method
                #  around in order to achieve
                #  better performance ...
                for col in color_ramp:
                    temp = (col - color) ** 2
                    temp_val = (np.sqrt(np.sum(temp)))
                    is_new_color = is_new_color and temp_val > 5

                if is_new_color:
                    _table.append((i, j))
                    color_ramp.append(color)

        # For each color get the respective
        #  representative class
        for i in range(0, len(_table)):
            pixel_pos = _table[i]

            # Get the color value
            #  and count the nb of
            #  occurrences on the image
            color = color_ramp[i]

            feature_url = f'http://mapas.dgterritorio.pt/wms-inspire/cos2018v1?language=por&SERVICE=WMS&VERSION=1.3.0' \
                          f'&REQUEST=GetFeatureInfo&FORMAT=image/png&TRANSPARENT=true&LAYERS=COS2018v1.0&STYLES' \
                          f'=default&WIDTH={image_size[0]}&HEIGHT={image_size[1]}&CRS={coordinate_format}&BBOX={bounds[0]},' \
                          f'{bounds[1]},{bounds[2]},{bounds[3]}&QUERY_LAYERS=COS2018v1.0&X={pixel_pos[0]}&Y={pixel_pos[1]}'

            req = urllib.request.Request(feature_url)
            with urllib.request.urlopen(req) as r:
                content = r.read().decode("utf-8")
                nb_of_instances = count_instances(hsv, color)

                name = content.split('\n')[6].split('\'')[1]
                class_names.append(name)

                if name not in final_values:
                    final_values[name] = []
                    final_counts[name] = []

                colors_value = final_values[name]
                colors_value.append(color)

                colors_count = final_counts[name]
                colors_count.append(nb_of_instances)

                final_values[name] = colors_value
                final_counts[name] = colors_count

        for c in class_names:
            index = np.argmax(final_counts[c], axis=0)
            ret_values[c] = final_values[c][index]

        print(ret_values)
        return ret_values, class_names

    def get_cos_data(
            self, bounds,
            target_f,
            image_size,
            coordinate_format='EPSG:3857'):

        # TODO: This is not the pretiest
        cos_link = f'http://mapas.dgterritorio.pt/wms-inspire/cos2018v1?language=por&SERVICE=WMS&VERSION=1.3.0' \
                   f'&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=COS2018v1.0&STYLES=def' \
                   f'ault&WIDTH={image_size[0]}&HEIGHT={image_size[1]}&CRS={coordinate_format}&BBOX={bounds[0]},' \
                   f'{bounds[1]},{bounds[2]},{bounds[3]} '

        req = urllib.request.Request(cos_link)  # , headers={'User-Agent': 'Mozilla/5.0'})
        with open(os.path.join(self.target_folder, target_f), "wb") as f:
            with urllib.request.urlopen(req) as r:
                f.write(r.read())
                print(f'Extracting data to {target_f}.')

        # TODO: Load table iterate through the
        #  image and store every new color you
        #  find, then also store the pixel position
        return self.__load_color_table(bounds, target_f, image_size)

    # Exports an image from the requested service
    #  and stores it in the target folder
    def get_image(
            self, layer,
            target_f,
            bounds,
            image_size,
            image_format,
            interpolation,
            pixel_type, type=True,
            operation='GET'):

        print('Start request')
        # Pool manager : This object handles all of the details
        #  of connection pooling and thread safety.
        http = urllib3.PoolManager()

        # Build the the necessary
        #  headers for the request
        query = self.__create_request(
            bounds, image_size, image_format.value, interpolation.value,
            pixel_type.value) if type else self.__create_request(
            bounds, image_size, image_format, interpolation, pixel_type)

        # Get the service url
        service_url = layer.value if type else layer

        # Send request, it will use
        #  GET request by default
        r = http.request(
            operation,
            service_url,
            preload_content=False,
            fields=query)

        if r.status == 200:
            print(f'Extracting data to {target_f}.')
            self.__store_data(r, target_f)
        elif r.status >= 400:
            print(f'Error, received code: {r.status}.')

    def get_table(self, table_name, table, is_json=True, type=True):
        # get the service url
        service_url = table.value if type else table

        # Pool manager : This object handles all of the details
        #  of connection pooling and thread safety.
        http = urllib3.PoolManager()

        # Send request, it will use
        #  GET request by default
        r = http.request('GET', service_url, preload_content=False) if not is_json else http.request(
            'GET', service_url, preload_content=False, fields={'f': 'pjson'})

        if r.status == 200:
            print(f'Extracting data to {table_name}.')
            with open(os.path.join(self.target_folder, table_name), 'w') as outfile:
                json.dump(r.data.decode('utf-8'), outfile)
        elif r.status >= 400:
            print(f'Error, received code: {r.status}.')
